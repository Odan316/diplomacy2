<?php

namespace frontend\components;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class GameModuleBootstrap
 *
 * Originally from: http://stackoverflow.com/questions/27954182/yii2-routes-definition-in-modules
 *
 * @package app\extensions
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
class GameModuleBootstrap implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $modules = $app->getModules();
        foreach ($modules as $moduleName => $properties) {
            if (is_array($properties) && isset($properties['type'])) {
                $sFilePathConfig = Yii::getAlias("@common/modules/{$moduleName}/config/_routes.php");
                if (file_exists($sFilePathConfig)) {
                    $app->getUrlManager()->addRules(require($sFilePathConfig));
                }
            }
        }
    }
}