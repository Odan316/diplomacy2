<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppLibsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower';
    public $baseUrl = '@web/assets';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
