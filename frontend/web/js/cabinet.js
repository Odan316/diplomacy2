$(function(){
    $('#create_game').on('click', function(){
        $("#cabinet_game_creation").modal();
    });

    $('.game_select').on('click', function(){
        var gameId = $(this).data('game-id');
        loadGame(gameId);
    });

    $(document).on('click', '.make_request', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/make-request?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#open_list').children('[data-game-id='+gameId+']').detach().clone().appendTo('#claimed_list');
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.request_accept', function(){
        var gameId = $('#b_game_info').data('game-id');
        var userId = $(this).data('user-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/accept-request?gameId=" + gameId + "&userId=" + userId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.request_refuse', function(){
        var gameId = $('#b_game_info').data('game-id');
        var userId = $(this).data('user-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/refuse-request?gameId=" + gameId + "&userId=" + userId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.start_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/start-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.cancel_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/cancel-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#gm_list').children('[data-game-id='+gameId+']').detach();
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.end_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: url_root + "cabinet/end-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#gm_list').children('[data-game-id='+gameId+']').detach();
                $('#cabinet_game_info').html(data);
            }
        });
    });
});

function loadGame(gameId) {
    $.ajax({
        type: "POST",
        async: false,
        url: url_root + "cabinet/get-game-info?gameId=" + gameId,
        data: {
            _csrf : $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            $('#cabinet_game_info').html(data);
        }
    });
}