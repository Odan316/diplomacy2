<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \frontend\models\SignupForm
 */

$this->title = Yii::t('frontend', Yii::$app->name) . ' - ' . Yii::t('frontend', 'Sign up');
?>
<div class="b_start b_reg block1">
    <h1 class="logo"><?= Yii::t('frontend', 'Diplomacy') ?></h1>

    <h3><?= Html::encode(Yii::t('frontend', 'Sign up')) ?></h3>
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'nickname') ?>
    <?= $form->field($model, 'mail') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Sign up'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="b_reg_start">
        <?= Html::a(Yii::t('frontend', 'Sign in'), ['/']) ?>
    </div>
</div>
