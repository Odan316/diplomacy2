<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('frontend', Yii::$app->name) . ' - ' . Yii::t('frontend', 'Online client');
?>

<div class="b_start block1">
    <h1 class="logo"><?= Yii::t('frontend', 'DiploGame') ?></h1>

    <h2><?= Yii::t('frontend', 'Sign in') ?></h2>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <?= $form->field($model, 'username')->label(Yii::t('frontend', 'Username')) ?>
    <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('frontend', 'Password')) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Sign in'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="b_reg_start">
        <?= Html::a(Yii::t('frontend', 'Sign up'), ['/signup']) ?>
    </div>
</div>
