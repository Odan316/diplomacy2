<?php
use common\models\Game;
use common\models\Module;
use common\models\User;
use frontend\assets\CabinetAsset;
use frontend\models\GameCreationForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $user User
 * @var $modules Module[]
 * @var $gameHtml string
 */

CabinetAsset::register($this);

$this->title = Yii::t('frontend', Yii::$app->name) . ' - ' . Yii::t('frontend', 'User cabinet');
?>

<div class="b_page_wrap">
    <div class="cabinet_left block2">
        <h2>
            <?= Html::encode($user->person->nickname); ?>
            <?= Html::a(Yii::t('frontend', 'Logout'), ['site/logout'],
                ['id' => 'cabinet_logout', 'class' => 'active_span', 'data' => ['method' => 'post']]) ?>
        </h2>

        <h3><?= Yii::t('frontend', 'Games, you master') ?>:</h3>

        <div id="gm_list" class="games_list">
            <?php foreach ($user->getMasterGames()->active()->all() as $game) { ?>
                <p class="game_select"
                   data-game-id="<?= $game->id ?>"><?= Html::encode($game->title) . "(" . Html::encode($game->tag) . ")" ?></p>
            <?php } ?>
        </div>

        <h3><?= Yii::t('frontend', 'Games, you play') ?>:</h3>

        <div class="games_list">
            <?php foreach ($user->getPlayerGames()->active()->all() as $game) { ?>
                <p class="game_select"
                   data-game-id="<?= $game->id ?>"><?= Html::encode($game->title) . "(" . Html::encode($game->tag) . ")" ?></p>
            <?php } ?>
        </div>

        <h3><?= Yii::t('frontend', 'Games, open for register') ?>:</h3>

        <div id="open_list" class="games_list">
            <?php foreach (Game::find()->dontHasUser()->open()->all() as $game) { ?>
                <p class="game_select"
                   data-game-id="<?= $game->id ?>"><?= Html::encode($game->title) . "(" . Html::encode($game->tag) . ")" ?></p>
            <?php } ?>
        </div>

        <h3><?= Yii::t('frontend', 'Games, you claim patricipation') ?>:</h3>

        <div id="claimed_list" class="games_list">
            <?php foreach ($user->getClaimerGames()->active()->all() as $game) { ?>
                <p class="game_select"
                   data-game-id="<?= $game->id ?>"><?= Html::encode($game->title) . "(" . Html::encode($game->tag) . ")" ?></p>
            <?php } ?>
        </div>
        <?= Html::button(Yii::t('frontend', 'New Game'), ['class' => 'btn btn-primary', 'id' => 'create_game']) ?>
    </div>
    <div class="cabinet_right block2">
        <h2><?= Yii::t('frontend', 'Game information') ?></h2>

        <div id="cabinet_game_info">
            <?= $gameHtml; ?>
        </div>
    </div>
</div>


<?php Modal::begin( [
    'id'     => 'cabinet_game_creation',
    'header' => '<h4 class="modal-title">' . Yii::t('frontend', 'New game creation') . '</h4>',
    'footer' => ''

] ); ?>
<div class="well">
    <?php
    $model = new GameCreationForm();
    $form = ActiveForm::begin([
        'id'      => 'game-creation-form',
        'options' => ['class' => 'form-horizontal'],
        'action'  => '/cabinet/create-game'
    ]); ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'moduleId')->dropDownList(ArrayHelper::map($modules, 'id', 'title')) ?>
    <?= Html::submitButton(Yii::t('frontend', 'Create'),
        ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
    <?php ActiveForm::end() ?>
</div>
<?php Modal::end(); ?>