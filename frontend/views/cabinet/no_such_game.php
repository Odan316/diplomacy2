<?php
/**
 * @var $this \yii\web\View
 */
use yii\helpers\Html;

$this->title = Yii::t('frontend', Yii::$app->name) . ' - ' . Yii::t('frontend', 'No such game');
?>
<div class="b_page_wrap block1">
    <div id="error_page_block" class="block2">
        <h2><?= Yii::t('frontend', 'There is no game with provided ID') ?></h2>
        <?= Html::a(Yii::t('frontend', 'Back to cabinet'), ['/cabinet'], ['id' => 'error_cabinet_link']) ?>
    </div>
</div>