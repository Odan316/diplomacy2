<?php
/**
 * @var $this \yii\web\View
 * @var $game Game
 * @var $master User
 * @var $players User[]
 * @var $claimers User[]
 * @var $userRole UserGameRole
 */
use common\models\Game;
use common\models\GameRole;
use common\models\GameStatus;
use common\models\User;
use common\models\UserGameRole;
use yii\helpers\Html;

?>
<div id="b_game_info" data-game-id="<?= $game->id; ?>">
    <h3><?= Html::encode($game->title); ?></h3>

    <p class="point_title"><?= Yii::t('frontend', 'Game Master') ?>:</p>

    <div class="point_content">
        <div class="point_row">
            <?= Html::encode(
                ( $master->person ? $master->person->nickname : "" ) .
                ( Yii::$app->getUser()->getId() == $master->id ? ' (' . Yii::t('frontend', 'You') . ')' : '' )); ?>
        </div>
    </div>
    <p class="point_title"><?= Yii::t('frontend', 'Players') ?>:</p>

    <div class="point_content" id="game_participants">
        <?php if ( ! empty( $players )) { ?>
            <?php foreach ($players as $player): ?>
                <div class="point_row">
                    <?= Html::encode($player->person ? $player->person->nickname : ""); ?>
                </div>
            <?php endforeach ?>
        <?php } else { ?>
            <div class="point_row">
                <p><?= Yii::t('frontend', 'No players') ?></p>
            </div>
        <?php } ?>
    </div>
    <p class="point_title"><?= Yii::t('frontend', 'Requests') ?>:</p>

    <div class="point_content" id="game_participants">
        <?php if ( ! empty( $claimers )) { ?>
            <?php foreach ($claimers as $claimer): ?>
                <div class="point_row" data-id="<?= $claimer->id ?>">
                    <?= Html::encode($claimer->person ? $claimer->person->nickname : ""); ?>
                    <?php if ( ! empty( $userRole ) && $userRole->roleId == GameRole::MASTER_ROLE) {
                        echo Html::button(Yii::t('frontend', 'Accept'), [
                            'class' => 'request_accept btn btn-xs btn-primary',
                            'data'  => ['user-id' => $claimer->id]
                        ]);
                        echo Html::button(Yii::t('frontend', 'Refuse'), [
                            'class' => 'request_refuse btn btn-xs btn-danger',
                            'data'  => ['user-id' => $claimer->id]
                        ]);
                    } ?>
                </div>
            <?php endforeach ?>
        <?php } else { ?>
            <div class="point_row">
                <p><?= Yii::t('frontend', 'No requests') ?></p>
            </div>
        <?php } ?>
    </div>
    <div id="game_actions">
        <?php if (empty( $userRole )) {
            echo Html::button(Yii::t('frontend', 'Make request'), ['class' => 'make_request btn btn-primary']);
        } elseif ($userRole->roleId == GameRole::MASTER_ROLE) {
            if ($game->statusId == GameStatus::OPEN_GAME) {
                echo Html::button(Yii::t('frontend', 'Start game'), ['class' => 'start_game btn btn-success']);
                echo Html::button(Yii::t('frontend', 'Cancel game'), ['class' => 'cancel_game btn btn-danger']);
            } elseif ($game->statusId == GameStatus::ACTIVE) {
                if ( ! empty( $game->module )) {
                    echo Html::a(Yii::t('frontend', 'To GM cabinet'),
                        //['/' . $game->module->systemName, 'game' => $game->id],
                    [$game->module->systemName . '/default/index', 'game' => $game->id],
                        ['class' => 'btn btn-primary']);
                }
                echo Html::button(Yii::t('frontend', 'End game'), ['class' => 'end_game btn btn-danger']);
            }
        } elseif ($userRole->roleId == GameRole::PLAYER_ROLE) {
            if ($game->statusId == GameStatus::ACTIVE) {
                if ( ! empty( $game->module )) {
                    echo Html::a(Yii::t('frontend', 'To Player cabinet'),
                        [$game->module->systemName . '/default/index', 'game' => $game->id],
                        ['class' => 'btn btn-primary']);
                }
            }
        }
        ?>
    </div>
</div>