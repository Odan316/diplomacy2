<?php
namespace app\modules\vestria\controllers;

use app\modules\vestria\components\ModelsFinder;
use app\modules\vestria\components\VesController;
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\forms\CharacterForm;
use app\modules\vestria\models\forms\FactionForm;
use app\modules\vestria\widgets\request_position\RequestPositionWidget;
use common\models\User;
use Yii;
use yii\helpers\VarDumper;
use yii\web\HttpException;

/**
 * Class AjaxController
 *
 * Контроллер для работы с AJAX-запросами
 */
class AjaxController extends VesController
{
    /**
     *
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return parent::beforeAction($action);
        } else {
            throw new HttpException(404, 'Document Does Not Exist');
        }
    }
    public function actionGetCharactersList()
    {
        $criteria = Yii::$app->request->post("criteria", []);

        $models = $this->game->getCharacters($criteria, 1);

        return $models;
    }

    public function actionGetCharacterData()
    {
        $id = Yii::$app->request->post( "id", 0 );

        $character = $this->game->getCharacter( $id );

        return $character;
    }

    public function actionGetCharacterDataByPlayerId()
    {
        $playerId = Yii::$app->request->post( "playerId", 0 );

        $character = $this->game->getCharacterByPlayerId( $playerId );

        return $character;
    }

    public function actionGetPlayerData()
    {
        return User::findOne( Yii::$app->request->post( "id", 0 ) );
    }

    public function actionGetTraitsByClassId()
    {
            $classId                    = Yii::$app->request->post("classId", 0);

            $list         = [];
            $traitsConfig = $this->game->getConfig()->getConfigAsArray('character_traits');
            foreach ($traitsConfig['elements'] as $key => $values) {
                if (in_array($classId, $values['classes'])) {
                    $list[] = ["id" => $values['id'], "name" => $values['name']];
                }
            }

            return $list;
    }

    public function actionGetTraitsList()
    {
            $id                         = Yii::$app->request->post("id", 0);
            if ( ! empty( $id )) {
                $character = $this->game->getCharacter($id);
            } else {
                $classId   = Yii::$app->request->post("classId", 0);
                $character = new Character($this->game);
                $character->setClassId($classId);
            }

            $models = (new ModelsFinder($this->game))->findTraits($character);

            $list = $this->game->makeList($models);

            return $list;
    }

    public function actionGetAmbitionsList()
    {
        $id = Yii::$app->request->post( "id", 0 );
        if(!empty($id)){
            $character = $this->game->getCharacter($id);
        } else {
            $classId = Yii::$app->request->post( "classId", 0 );
            $character = new Character($this->game);
            $character->setClassId($classId);
        }

        $models = (new ModelsFinder($this->game))->findAmbitions($character);

        $list = $this->game->makeList($models);

        return $list;
    }

    public function actionSaveCharacter()
    {
        $form = new CharacterForm();
        $form->load(Yii::$app->request->post());
        return $form->apply();
    }

    public function actionGetFactionData()
    {
        return $this->game->getFaction( Yii::$app->request->post( "id", 0 ) );
    }

    public function actionSaveFaction()
    {
        $form = new FactionForm();
        $form->load(Yii::$app->request->post());
        return $form->apply();
    }

    public function actionGetProvinceData()
    {
        return $this->game->getProvince( Yii::$app->request->post( "id", 0 ) );
    }

    public function actionSaveProvince()
    {
        $data = Yii::$app->request->post( "Province", [ ] );
        if ( ! empty( $data['id'] )) {
            echo $this->game->updateProvince( $data );
        } else {
            echo false;
        }
    }

    public function actionGetRequestPositionCode()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

        $character = $this->game->getCharacterByPlayerId(Yii::$app->getUser()->getId());
        $actions = (new ModelsFinder($this->game))->findActions($character);
        echo $this->renderAjax('request_position', ['actions' => $actions]);
    }

    public function actionGetActionParametersCode()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

        $actionId = Yii::$app->request->post( "actionId", 0 );
        if(!empty($actionId)){
            $character = $this->game->getCharacterByPlayerId(Yii::$app->getUser()->getId());
            /** @var CharacterAction $action */
            $action = $this->game->getConfig()->getConfigElementById("character_actions", $actionId);

            echo $this->renderAjax("action_parameters", ["action" => $action, "character" => $character]);
        } else {
            echo "";
        }
    }

    public function actionDeletePosition()
    {
        $id = Yii::$app->request->post( "id", 0 );
        $character = $this->game->getCharacterByPlayerId(Yii::$app->getUser()->getId());
        $this->game->getRequestByCharacterId($character->getId())->deletePosition($id);
    }

    public function actionSaveRequest()
    {
        $positions = Yii::$app->request->post( "positions", [ ] );
        $character = $this->game->getCharacterByPlayerId( Yii::$app->getUser()->getId() );
        if(!empty($character)){
            $data = ["characterId" => $character->getId(), "positions" => $positions];
            $request = $this->game->getRequestByCharacterId($character->getId());
            if(!empty($request)){
                echo $this->game->updateRequest( $data );
            } else {
                $model = $this->game->createRequest( $data );
                echo (!empty($model));
            }
        } else {
            echo false;
        }
    }
}