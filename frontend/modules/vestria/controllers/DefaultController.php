<?php
namespace app\modules\vestria\controllers;

use app\modules\vestria\components\ModelsFinder;
use app\modules\vestria\components\VesController;
use app\modules\vestria\models\Log;
use common\models\GameRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class GameController
 *
 * Контроллер для работы в Кабинете (Ведущего или Игрока)
 */
class DefaultController extends VesController
{
    /**
     * По умолчанию, в зависимости от роли мы грузим
     * - либо страницу ГМа (для ГМа),
     * - либо страницу игрока (для остальных)
     */
    public function actionIndex()
    {
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {
            return $this->actionGM();
        } else {
            return $this->actionPlayer();
        }
    }

    /**
     * Страница ГМа (только для ГМа)
     */
    public function actionGM()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {

            return $this->render( 'gm/index', [
                'game' => $this->game,
                'players'     => $this->gameModel->getPlayers()->all(),
                'classesList' => $this->game->getConfig()->getConfigAsList( "character_classes" ),
                'provincesList' => ArrayHelper::map($this->game->getProvinces([], true), 'id', 'name'),
                'mapSVG' => $this->game->getMap()->getSVG()
            ] );
        } else {
            return $this->actionAccessDenied();
        }
    }

    /**
     * Страница игрока
     */
    public function actionPlayer()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::PLAYER_ROLE) {

            $character = $this->game->getCharacterByPlayerId(Yii::$app->getUser()->getId());
            if(!$character){
                return $this->render( 'player/setup', [
                    'classesList' => $this->game->getConfig()->getConfigAsList( "character_classes" ),
                    'provincesList' => $this->game->getConfig()->getConfigAsList( "provinces" )
                ] );
            } else {
                return $this->render( 'player/index', [
                    'character' => $character,
                    'actions' => (new ModelsFinder($this->game))->findActions($character),
                    'request' => $this->game->getRequestByCharacterId($character->getId())
                ] );
            }
        } else {
            return $this->actionAccessDenied();
        }
    }

    public function actionRules()
    {
        return $this->render( 'rules' );
    }

    /**
     * Отображение заглушки, говорящей что страница не доступна этой роли
     */
    public function actionAccessDenied()
    {
        return $this->render( 'access_denied' );
    }
}