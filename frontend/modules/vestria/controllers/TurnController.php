<?php
namespace app\modules\vestria\controllers;

use app\modules\vestria\components\VesController;
use app\modules\vestria\components\Battle;
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\RequestPosition;
use Yii;

/**
 * Class TurnController
 *
 * Контроллер для управления обсчетом хода
 */
class TurnController extends VesController
{

    const ESTATES_BASE_INCOME = 50;
    const FACTORIES_BASE_INCOME = 20;
    const ARMY_BASE_UPKEEP = 1;

    /**
     * Запуск обсчета хода
     */
    public function actionGo()
    {
        $this->makeTurn();

        $this->redirect( ['/vestria/default/index'] );
    }

    /**
     * Управление обсчетом хода
     */
    private function makeTurn()
    {
        // предобработка хода
        $this->preprocessTurn();
        // общие заявки
        $this->applicateRequests([CharacterAction::PHASE_COMMON]);
        // переводы и "быстрые доходы"
        $this->applicateRequests([CharacterAction::PHASE_IMMEDIATE_INCOME]);
        // покупки и набор войск
        $this->applicateRequests([CharacterAction::PHASE_SPENDING]);
        // приход в бюджет
        $this->budgetSpending();
        // движения армий и сражения
        $this->manoeuvresAndBattles();
        // проверка принадлежности провинций
        //$this->provinceCheck();
        // движения армий и сражения
        $this->applicateRequests([CharacterAction::PHASE_AFTER_MANOEUVRES]);
        // приход в бюджет
        $this->budgetIncome();
        // приход в бюджет и набор рекрутов от действий
        $this->applicateRequests([CharacterAction::PHASE_INCOME]);
        // проверка победных условий
        //$this->checkAmbitions();
        // постобработка хода
        $this->postprocessTurn();
    }

    /**
     * Предобработка хода:
     * - переключение номера хода
     * - рандомизирование списка персонажей для рандомизирования порядка хода
     */
    private function preprocessTurn()
    {
        $this->game->getLog()->addRow(Yii::t('vestria', 'Turn number')." ".$this->game->getTurn());

        $this->game->setTurn($this->game->getTurn() + 1);
        $this->gameModel->lastTurn += 1;
        $this->game->randomizeCharactersOrder();

        /* Logging */
        $logRow = "<p>".Yii::t('vestria', 'New requests order:')."</p><ul>";
        foreach($this->game->getCharacters() as $key => $character){
            $logRow .= "<li>".$key.": ".$character->getPlayer()->person->nickname.' - '.$character->getName()."</li>";
        }
        $logRow .= "</ul>";
        $this->game->getLog()->addRow($logRow);
    }

    /**
     * Постобработка хода:
     * - очистка списка заявок
     * - сохранение нового файла хода
     * - сохранение модели игры в платформе
     */
    private function postprocessTurn()
    {
        $this->game->clearRequests();
        $this->game->save();
        $this->gameModel->save();
    }


    /**
     * Применение позиций заявок заданных типов
     * @param int[] $phases
     */
    private function applicateRequests($phases = [])
    {
        $phasesNames = [];
        foreach($phases as $phase){
            $phasesNames[] = CharacterAction::getPhases()[$phase];
        }
        $this->game->getLog()->addRow(Yii::t('vestria',
                'Applicate requests').' '.Yii::t('vestria', '{n, plural, =1{phase} other{phases}} {num}',
                ['num' => implode(', ', $phasesNames), 'n' => count($phases)]));
       // \CVarDumper::dump(json_encode($this->game), 3, 1);
        foreach($this->game->getCharacters() as $character) {
            $positions = $this->getPositionsByPhases($character, $phases);

            /** @var RequestPosition $position */
            foreach($positions as $position){
                $position->apply();
            }

        }
        //\CVarDumper::dump(json_encode($this->game), 3, 1);
        //die();
    }

    /**
     * @param Character $character
     * @param int[] $phases
     *
     * @return RequestPosition[]
     */
    private function getPositionsByPhases($character, $phases)
    {
        $request = $this->game->getRequestByCharacterId($character->getId());
        $positions = [];
        if(is_object($request))
            $positions = $request->getPositions(['action.phase' => $phases]);

        return $positions;
    }


    /**
     * Расчет рутинных расходов бюджета
     * - за армию эквивалентно численности
     */
    private function budgetSpending()
    {
        $this->game->getLog()->addRow(Yii::t('vestria', 'Calculate budget spendings'));

        foreach($this->game->getCharacters() as $character) {
            // Army upkeep
            if(is_object($character->getArmy())){
                $armyStrength = $character->getArmy()->getStrength();
                $armyUpkeepModifier = $character->getModifier("armyUpkeep");
                $armyUpkeep = $armyStrength * self::ARMY_BASE_UPKEEP * (1+$armyUpkeepModifier);

                $character->setCash($character->getCash() - $armyUpkeep);
            }
        }
    }

    /**
     * Расчет передвижений и битв
     * - двигаются армии по одной
     * - если в провинции назначения есть армия чужой фракции - происходит бой
     */
    private function manoeuvresAndBattles()
    {
        foreach($this->game->getCharacters() as $character) {
            $positions = $this->getPositionsByPhases($character, [CharacterAction::PHASE_MANOEUVRES]);
            foreach($positions as $position){
                $position->apply();
                $commandersInProvince = $this->game->getCharacters(["provinceId" => $character->getProvinceId(), "armyId" => ["notEmpty"]]);
                $needBattle = false;
                foreach($commandersInProvince as $commander){
                    foreach($commandersInProvince as $commander2){
                        if($commander->getFactionId() != $commander2->getFactionId()){
                            $needBattle = true;
                            break;
                        }
                    }
                    if($needBattle)
                        break;
                }
                if($needBattle){
                    $battle = new Battle($character->getProvince());
                    $battle->setAttacker($character);
                    $battle->calculateResult();
                }
            }

        }
    }

    /**
     * Расчет рутинных доходов бюджета
     * - за каждое владение
     * - за каждое предприятие
     */
    private function budgetIncome()
    {
        foreach($this->game->getCharacters() as $character) {
            $estatesModifier = $character->getModifier("estatesIncome");
            $estatesIncome = $character->getEstatesCount() * self::ESTATES_BASE_INCOME * (1+$estatesModifier);

            $factoriesModifier = $character->getModifier("factoriesIncome");
            $factoriesIncome = $character->getFactoriesCount() * self::FACTORIES_BASE_INCOME * (1+$factoriesModifier);

            $character->setCash($character->getCash() + $estatesIncome + $factoriesIncome);
        }
    }

    private function provinceCheck()
    {

    }

    private function checkAmbitions()
    {

    }
}