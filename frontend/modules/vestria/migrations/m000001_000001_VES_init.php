<?php

use yii\db\Migration;

class m000001_000001_VES_init extends Migration
{
	public function safeUp()
	{
		$this->insert('{{%module}}', [
			'title' => 'Вестрия: Время перемен',
			'tag' => 'VES',
			'authorId' => 1,
			'systemName' => 'vestria',
            'active' => true
		]);
	}

	public function safeDown()
	{
		$this->delete('{{%module}}', ['tag' => 'VES']);
	}
}