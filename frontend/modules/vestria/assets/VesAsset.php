<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\vestria\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class VesAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebroot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'css/vestria.css',
    ];
    public $js = [
        'js/common.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\vestria\assets\VesLibsAsset',
    ];
}
