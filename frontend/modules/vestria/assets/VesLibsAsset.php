<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\vestria\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class VesLibsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/';
    public $baseUrl = '@web/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];
    //public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    ];
    public $js = [
        'jquery-validate/dist/jquery.validate.min.js'
    ];
    public $depends = [
    ];
}
