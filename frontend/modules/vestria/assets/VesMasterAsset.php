<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\vestria\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class VesMasterAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebroot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
    ];
    public $js = [
        'js/gm.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\vestria\assets\VesAsset',
    ];
}
