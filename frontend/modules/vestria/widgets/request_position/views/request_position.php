<?php
use app\modules\vestria\widgets\position_parameters\PositionParametersWidget;
use app\modules\vestria\widgets\request_position\RequestPositionAsset;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\RequestPosition;
use frontend\components\JSONModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

$widgetAssets = RequestPositionAsset::register($this);

/**
 * @var $this View
 * @var $actions CharacterAction[]
 * @var $position RequestPosition|null
 * @var $i int
 */
?>
<?php
$positionId = !empty($position) ? $position->getId() : 0;
$actionsListData = ArrayHelper::merge([0 => "Выберите"], ArrayHelper::map(JSONModel::makeList($actions), "id", "name"));
?>
<div class="request_block">
    <?= Html::button('<img src="'.$widgetAssets->baseUrl.'/images/icon_cancel.png" />',
        ['class' => 'delete_request_position', 'name' => 'delete_request_position']) ?>
    <?= Html::hiddenInput('id', $positionId, ["id" => "p".$positionId."Id", "class" => "positionId"]) ?>
    <label><span class="position_num"><?= $i ?></span>:
        <?= Html::dropDownList(
            "requests",
            (!empty($position) ? $position->getActionId() : 0),
            $actionsListData,
            ["id" => "p".$positionId."Pos", "class" => "reguest_position"]) ?>
    </label>
    <div class="request_params">
    <?php if(!empty($position)){?>

        <?= PositionParametersWidget::widget(
            [
                "action"     => $position->getAction(),
                "positionId" => $position->getId(),
                "character"  => $position->getRequest()->getCharacter()
            ]) ?>
    <?php } ?>
    </div>
</div>