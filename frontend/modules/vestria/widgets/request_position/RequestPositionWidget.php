<?php
namespace app\modules\vestria\widgets\request_position;

use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\RequestPosition;
use yii\base\Widget;

/**
 * Class RequestPositionWidget
 * @package app\modules\vestria\widgets
 */
class RequestPositionWidget extends Widget
{
    /** @var CharacterAction[] */
    public $actions = [];
    /** @var RequestPosition */
    public $position = null;
    /** @var int */
    public $i = 0;

    public function init()
    {
    }

    public function run()
    {
        return $this->render( "request_position", ['actions' => $this->actions, 'position' => $this->position, 'i' => $this->i] );
    }


}