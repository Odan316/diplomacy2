<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\vestria\widgets\request_position;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RequestPositionAsset extends AssetBundle
{
    public $sourcePath = '@vestria/widgets/request_position/';
    public $baseUrl = '@web/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/request_position.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
