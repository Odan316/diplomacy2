<?php
namespace app\modules\vestria\widgets\position_parameters;

use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use yii\base\Widget;

/**
 * Class PositionParametersWidget
 * @package diplomacy\modules\vestria\widgets
 */
class PositionParametersWidget extends Widget
{
    /** @var CharacterAction */
    public $action;
    /** @var int */
    public $positionId;
    /** @var Character */
    public $character;

    public function init()
    {
    }

    public function run()
    {
        $request = $this->character->getRequest();
        if(is_object($request))
            $position = $request->getPosition($this->positionId);
        if(isset($position) && is_object($position))
            $values = $position->getParameters();
        else
            $values = [];

        return $this->render( "position_parameters", ["values" => $values, 'action' => $this->action, 'character' => $this->character] );
    }
}