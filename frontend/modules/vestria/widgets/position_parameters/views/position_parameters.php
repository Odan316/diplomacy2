<?php
/**
 * @var $this View
 * @var $values array
 * @var $action CharacterAction
 * @var $character Character
 */
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use yii\web\View;

?>
<?php
foreach($action->getParameters() as $parameter){
    $value = isset($values[$parameter->getName()]) ? $values[$parameter->getName()] : null; ?>
    <?= $parameter->getParameterCode($character, $value); ?>
<?php } ?>