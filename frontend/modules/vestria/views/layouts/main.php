<?php
/**
 * @var $this \yii\web\View
 * @var string $content
 */
use app\modules\vestria\assets\VesAsset;
use yii\helpers\Html;
VesAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        window.url_root = '<?= \Yii::$app->controller->module->params['moduleJSUrl']?>';
    </script>
    <link href='http://fonts.googleapis.com/css?family=Marck+Script&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
</head>

<body>
<?php $this->beginBody() ?>
<div id="page" class="container">
    <div id="inner_content">
        <?= $content; ?>
    </div>

    <div class="clearfix"></div>

    <div id="footer">
        "<?=Yii::$app->name ?>" Copyright by Onad &copy; <?= date('Y'); ?>. No Rights Reserved.
        <?= Html::a(Yii::t('vestria', 'Back to cabinet'), ['/cabinet'], ['id' => 'cabinet_link']) ?>
        <br/>
		<?= Yii::powered(); ?>
	</div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
