<?php
/**
 * @var $this View
 * @var $actions CharacterAction[]
 */
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\widgets\request_position\RequestPositionWidget;
use yii\web\View;

?>

<?= RequestPositionWidget::widget([ "actions" => $actions, "position" => null, "i" => '' ]); ?>