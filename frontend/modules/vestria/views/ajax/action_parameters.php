<?php
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\widgets\position_parameters\PositionParametersWidget;
use yii\web\View;

/**
 * @var $this View
 * @var $action CharacterAction
 * @var $character Character
 * @var $action CharacterAction
 */
?>

<?= PositionParametersWidget::widget([ "action" => $action, "positionId" => 0, "character" => $character ]); ?>
