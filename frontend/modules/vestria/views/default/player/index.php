<?php
use app\modules\vestria\assets\VesPlayerAsset;
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\Request;
/**
 * @var $this \yii\web\View
 * @var $character Character
 * @var $actions CharacterAction[]
 * @var $request Request
 */
VesPlayerAsset::register($this);

$this->title = Yii::t('vestria', \Yii::$app->controller->module->title) . ' - ' . Yii::t('vestria', 'Player cabinet');
?>

<div id="left_panel">
    <h2><?= Yii::t('vestria', 'Player info') ?></h2>
    <p><?= Yii::t('vestria', 'Turn') ?>: <span><?= Yii::$app->controller->game->getTurn(); ?></span></p>
    <h4><?= $character->getName(); ?> (<?= $character->getClass()->getName(); ?>)</h4>
    <p><?= Yii::t('vestria', 'Now in province') ?>: <span><?= $character->getProvince()->getName(); ?></span></p>
    <p><?= Yii::t('vestria', 'Faction') ?>: <span>
            <?php if( $character->getFaction()){
                echo $character->getFaction()->getName();
                if($character->getFaction()->getLeaderId() == $character->getId()){
                    echo " (".Yii::t('vestria', 'Leader').")";
                }
            } else
                echo Yii::t('vestria', 'No');
             ?>
        </span></p>
    <p><?= Yii::t('vestria', 'Trait') ?>: <span><?= $character->getTrait()->getName() ?></span></p>
    <p><?= Yii::t('vestria', 'Ambition') ?>: <span><?= $character->getAmbition()->getName() ?></span></p>
    <p><?= Yii::t('vestria', 'Popularity') ?>: <span><?= $character->getPopularity() ?></span></p>
    <p><?= Yii::t('vestria', 'Wealth') ?>: <span><?= $character->getCash() ?> <?= Yii::t('vestria', 'vesCurrency') ?></span></p>
    <p><?= Yii::t('vestria', 'Recruits') ?>: <span><?= $character->getRecruits() ?></span></p>
    <?php if ($character->getEstatesCount()) { ?>
        <p><?= Yii::t('vestria', 'Estates') ?>: <span>
                <?= $character->getEstatesCount() ?>
                <?php if ($character->getModifier( "estatesIncome" ) > 0) { ?>
                    (<?= Yii::t('vestria', 'Income Modifier') ?>: <?= ( 1 + $character->getModifier( "estatesIncome" ) ) * 100 ?>%)
                <?php } ?>
                </span>
        </p>
    <?php } ?>
    <?php if($character->getFactoriesCount()) {?>
        <p><?= Yii::t('vestria', 'Factories') ?>: <span><?= (string)$character->getFactoriesCount() ?>
                <?= $character->getFactoriesCount() ?>
                <?php if ($character->getModifier( "factoriesIncome" ) > 0) { ?>
                    (<?= Yii::t('vestria', 'Income Modifier') ?>: <?= ( 1 + $character->getModifier( "factoriesIncome" ) ) * 100 ?>%)
                <?php } ?>
            </span>
        </p>
    <?php } ?>
    <?php if($character->getArmy()) {?>
        <p>
            <?= Yii::t('vestria', 'Army') ?>: <span>
                <?= $character->getArmy()->getName(); ?>
                (<?= Yii::t('vestria/army', 'Strength') ?>: <?= $character->getArmy()->getStrength(); ?> / <?= Yii::t('vestria', 'Morale') ?>: <?= $character->getArmy()->getMorale(); ?> )
            </span>
        </p>
    <?php } ?>
</div>
<div id="central_panel">
    <div id="players_map">
        <?= Yii::$app->controller->game->getMap()->getSVG(550);?>
    </div>
    <div id="players_map_legend">
    </div>
</div>
<?= $this->render( "_requests",
    [ "character" => $character,  "actions" => $actions, "request" => $request ], 1 ) ?>
<div class="clearfix"></div>