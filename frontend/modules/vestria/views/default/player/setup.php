<?php
/**
 * @var $this \yii\web\View
 * @var $classesList [] Список доступных классов для персонажей
 * @var $provincesList [] Список доступных провинций
 */

use app\modules\vestria\assets\VesPlayerSetupAsset;
use app\modules\vestria\models\forms\CharacterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

VesPlayerSetupAsset::register($this);

$this->title = Yii::t('vestria', \Yii::$app->controller->module->title) . ' - ' . Yii::t('vestria', 'New character');
?>
<div id="new_character">
    <h2><?= Yii::t('vestria', 'New character creation')?></h2>
    <?php
    $model = new CharacterForm();
    $form = ActiveForm::begin([
        'id'      => 'character-form',
        'options' => ['class' => 'form-horizontal'],
        'action'  => '/'
    ]); ?>
    <?= $form->field($model, 'playerId', ['options' => ['class' => 'hidden']])
        ->hiddenInput(['value' => Yii::$app->getUser()->getId()])->label(false); ?>
    <?= $form->field($model, 'name')->label(Yii::t('vestria', 'Name')) ?>
    <?= $form->field($model, 'classId')->dropDownList($classesList)
             ->label(Yii::t('vestria', 'Class')) ?>
    <?= $form->field($model, 'traitId')->dropDownList([])
             ->label(Yii::t('vestria', 'Trait')) ?>
    <?= $form->field($model, 'ambitionId')->dropDownList([])
             ->label(Yii::t('vestria', 'Ambition')) ?>
    <?= $form->field($model, 'provinceId')->dropDownList($provincesList)
             ->label(Yii::t('vestria', 'Province')) ?>
    <?= Html::submitButton(Yii::t('vestria', 'Create'),
        ['class' => 'btn btn-primary', 'name' => 'but_character_save_new']) ?>
    <?php ActiveForm::end() ?>
</div>