<?php
use app\modules\vestria\assets\VesPlayerAsset;
use app\modules\vestria\models\Character;
use app\modules\vestria\models\CharacterAction;
use app\modules\vestria\models\Request;
use app\modules\vestria\widgets\request_position\RequestPositionWidget;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $character Character
 * @var $actions CharacterAction[]
 * @var $request Request
 */

$playerAssets = VesPlayerAsset::register($this);
?>
<div id="right_panel" class="requests_list">
    <h2><?= Yii::t('vestria', 'Request') ?></h2>
    <form id="requests_form">
    <?php
    $i = 0;
    if ( ! empty( $request )) {
        foreach ($request->getPositions() as $position) {
            $i ++;
            ?>
            <?= RequestPositionWidget::widget([ "actions" => $actions, "position" => $position, "i" => $i ]) ?>
        <?php }
    } ?>
    <?= Html::button('<img src="'.$playerAssets->baseUrl.'/images/design/add.png" />',
        ['class' => 'add_request_position', 'name' => 'add_request_position']) ?>
    </form>
    <div class="clearfix"></div>
    <?= Html::button(Yii::t('vestria', 'Save'),
        ['class' => 'btn btn-primary but_request_save', 'name' => 'but_request_save']) ?>
</div>
