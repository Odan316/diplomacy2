<?php
/**
 * @var $this DefaultController
 */

use app\modules\vestria\controllers\DefaultController;

$this->title = Yii::t('vestria', \Yii::$app->controller->module->title) . ' - ' . Yii::t('vestria', 'Access is denied');
?>
<h2><?= Yii::t('project13', 'Your role doesn\'t give you access to this page') ?></h2>