<?php
/**
 * @var $this \yii\web\View
 */
use app\modules\vestria\models\forms\ProvinceForm;
use app\modules\vestria\models\Province;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>
<div id="gm_map_outer">
    <?= Yii::$app->controller->game->getMap()->getSVG(); ?>
</div>
<div id="gm_provinces_list">
    <h2><?= Yii::t('vestria', 'Provinces') ?></h2>
    <?php /** @var Province $province */
    foreach (Yii::$app->controller->game->getProvinces() as $province) { ?>
        <p data-province-id="<?= $province->getId() ?>">
            <?= $province->getName() ?>
            <span>(<?= ( $province->getOwner()
                    ? $province->getOwner()->getName()
                    : '<span class="alert-error">'.Yii::t('vestria', 'No faction').'</span>' ) ?>)
            </span>
            <?= Html::button(Yii::t('vestria', 'Edit'),
                ['class' => 'btn btn-primary btn-sm edit_province_gm', 'name' => 'edit_province_gm']) ?>
        </p>
    <?php } ?>
</div>

<?php Modal::begin([
    'id'     => 'edit_province_gm',
    'header' => '<h4 class="modal-title">' . Yii::t('vestria', 'Province') . '</h4>',
    'footer' => ''

]); ?>
    <div class="well">
        <?php
        $model = new ProvinceForm();
        $form  = ActiveForm::begin([
            'id'      => 'province-form',
            'options' => ['class' => 'form-horizontal'],
            'action'  => '/'
        ]); ?>
        <?= $form->field($model, 'id', ['options' => ['class' => 'hidden']])->hiddenInput() ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'ownerId')
            ->dropDownList(ArrayHelper::merge(
                ['' => Yii::t('vestria', 'No')],
                ArrayHelper::map(Yii::$app->controller->game->getFactions([], true), "id", "name")
            )) ?>
        <?= Html::submitButton(Yii::t('vestria', 'Save'),
            ['class' => 'btn btn-primary', 'name' => 'but_province_save_gm']) ?>
        <?php ActiveForm::end() ?>
    </div>
<?php Modal::end(); ?>