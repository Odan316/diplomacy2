<?php
use app\modules\vestria\models\Game;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @var $this \yii\web\View
 * @var $game Game
 */
?>
<div id="gm_turn_actions">
    <h3><?= Yii::t('vestria', 'Turn') ?>: <?= $game->getTurn(); ?></h3>

    <?= Html::a(Yii::t('vestria', 'Make turn'), ['turn/go'], ['class' => 'btn btn-danger btn-lg']) ?>
</div>
<div id="gm_turnLog">
    <h3><?= Yii::t('vestria', 'Last turn log') ?></h3>
    <?php
    foreach ($game->getLog($game->getTurn() - 1)->getRows() as $row) { ?>
        <p><?= $row->getText() ?></p>
    <?php } ?>
</div>