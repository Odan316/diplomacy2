<?php
/**
 * @var $this \yii\web\View
 * @var $players User[] Список игроков, с племенами
 * @var $classesList [] Список доступных классов для персонажей
 * @var $provincesList [] Список доступных провинций
 */
use app\modules\vestria\models\Character;
use app\modules\vestria\models\forms\CharacterForm;
use app\modules\vestria\models\Faction;
use app\modules\vestria\models\forms\FactionForm;
use common\models\User;
use kartik\color\ColorInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>
<div class="row">
    <div id="gm_players_list" class="col-md-6">
        <h2><?= Yii::t('vestria', 'Players') ?></h2>
        <?= Html::hiddenInput( 'player_id' ) ?>
        <?php foreach ($players as $player) { ?>
            <p data-player-id="<?= $player->id ?>">
                <?= $player->person->nickname ?>
                <?php
                /** @var Character $character */
                $character = Yii::$app->controller->game->getCharacterByPlayerId( $player->id ); ?>
                <?php if ( ! empty( $character )) { ?>
                    <span>(<?= $character->getName(); ?>
                        - <?= ( $character->getFaction() ? $character->getFaction()->getName() : Yii::t('vestria', 'No faction') ) ?>)</span>
                    <?= Html::button(Yii::t('vestria', 'Edit'),
                        ['class' => 'btn btn-primary btn-sm edit_character_gm', 'name' => 'edit_character_gm']) ?>
                <?php } else { ?>
                    <?= Html::button(Yii::t('vestria', 'Add'),
                        ['class' => 'btn btn-primary btn-sm add_character_gm', 'name' => 'add_character_gm']) ?>
                <?php } ?>
            </p>
        <?php } ?>
    </div>
    <div id="gm_factions_list" class="col-md-6">
        <h2><?= Yii::t('vestria', 'Factions') ?></h2>
        <?= Html::hiddenInput( 'faction_id' ) ?>
        <?php
        /** @var Faction $faction */
        foreach (Yii::$app->controller->game->getFactions() as $faction) { ?>
            <p data-faction-id="<?= $faction->getId() ?>">
                <?= $faction->getName() ?>
                <span>(<?=
                    ( $faction->getLeader() ? $faction->getLeader()->getName() : '<span class="alert-error">'.Yii::t('vestria', 'No leader').'</span>' )
                    ?>)</span>
                <?= Html::button(Yii::t('vestria', 'Edit'),
                    ['class' => 'btn btn-primary btn-sm edit_faction_gm', 'name' => 'edit_faction_gm']) ?>
            </p>
        <?php } ?>
        <?= Html::button(Yii::t('vestria', 'Add'),
            ['class' => 'btn btn-primary btn-sm add_faction_gm', 'name' => 'add_faction_gm']) ?>
    </div>
</div>

<?php Modal::begin([
    'id'     => 'edit_character_gm',
    'header' => '<h4 class="modal-title">' . Yii::t('vestria', 'Character') . '</h4>',
    'footer' => ''

]); ?>
    <div class="well">
        <?php
        $model = new CharacterForm();
        $form  = ActiveForm::begin([
            'id'      => 'character-form',
            'options' => ['class' => 'form-horizontal'],
            'action'  => '/'
        ]); ?>
        <div class="form-group">
            <label><?= Yii::t('vestria', 'Player') ?>:</label>
            <p id="character_player_name"></p>
        </div>
        <?= $form->field($model, 'playerId', ['options' => ['class' => 'hidden']])->hiddenInput() ?>
        <?= $form->field($model, 'id', ['options' => ['class' => 'hidden']])->hiddenInput() ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'classId')->dropDownList($classesList)?>
        <?= $form->field($model, 'traitId')->dropDownList([])?>
        <?= $form->field($model, 'ambitionId')->dropDownList([])?>
        <?= $form->field($model, 'provinceId')->dropDownList($provincesList)?>
        <?= Html::submitButton(Yii::t('vestria', 'Save'),
            ['class' => 'btn btn-primary', 'name' => 'but_character_save_new']) ?>
        <?php ActiveForm::end() ?>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id'     => 'edit_faction_gm',
    'header' => '<h4 class="modal-title">' . Yii::t('vestria', 'Faction') . '</h4>',
    'footer' => ''

]); ?>
    <div class="well">
        <?php
        $model = new FactionForm();
        $form  = ActiveForm::begin([
            'id'      => 'faction-form',
            'options' => ['class' => 'form-horizontal'],
            'action'  => '/'
        ]); ?>
        <?= $form->field($model, 'id', ['options' => ['class' => 'hidden']])->hiddenInput()?>
        <?= $form->field($model, 'name')?>
        <?= $form->field($model, 'leaderId')->dropDownList([]) ?>
        <?= $form->field($model, 'color')->widget(ColorInput::classname());?>
        <?= Html::submitButton(Yii::t('vestria', 'Save'),
            ['class' => 'btn btn-primary', 'name' => 'but_faction_save_gm']) ?>
        <?php ActiveForm::end() ?>
    </div>
<?php Modal::end(); ?>