<?php
use app\modules\vestria\assets\VesMasterAsset;
use app\modules\vestria\models\Game;
use common\models\User;

/**
 * @var $this \yii\web\View
 * @var $game Game
 * @var $players User[] Список игроков
 * @var $classesList [] Список доступных классов для персонажей
 * @var $provincesList [] Список доступных провинций
 */
VesMasterAsset::register($this);

$this->title = Yii::t('vestria', \Yii::$app->controller->module->title) . ' - ' . Yii::t('vestria', 'Master cabinet');
?>

<?=
\yii\bootstrap\Tabs::widget([
    'navType' => 'nav-pills',
    'items' => [
        [
            'active'  => true,
            'label'   => Yii::t('vestria', 'Turn'),
            'options' => ['id' => 'turn'],
            'content' => $this->render("_turn", ["game" => $game])
        ],
        [
            'label'   => Yii::t('vestria', 'Players and factions'),
            'options' => ['id' => 'players'],
            'content' => $this->render('_players',
                ["players" => $players, "classesList" => $classesList, "provincesList" => $provincesList])
        ],
        [
            'label'   => Yii::t('vestria', 'Provinces'),
            'options' => ['id' => 'provinces'],
            'content' => $this->render("_map", ["provincesList" => $provincesList])
        ]
    ]
]);
?>

<div class="clearfix"></div>