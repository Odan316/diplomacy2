$(function() {
    refreshCharacterLists($("#character-classid").val());
    $(document).on('submit', '#character-form', function (e) {
        e.preventDefault();
        var data = readCharacterForm();
        saveObject("character", data);
    });
    $(document).on('change', '#character-classid', function(){
        var classId = $(this).val();
        refreshCharacterLists(classId);
    });
});

function readCharacterForm()
{
    return $("#character-form").serializeArray();
}

function refreshCharacterLists(classId, traitId, ambitionId)
{
    var traits = getTraitsByClassId(classId);
    createList("character-traitid", traitId, traits);
    var ambitions = getAmbitionsByClassId(classId);
    createList("character-ambitionid", ambitionId, ambitions);
}