$(function(){
    $(document).on('click', '.add_character_gm, .edit_character_gm', function(){
        var playerId = $(this).parents("p").data("player-id");
        var playerData = getObjectData("player", playerId);
        var characterData = getCharacterDataByPlayerId(playerId);
        if(characterData == null){
            refreshCharacterLists(0, 1);
        }
        fillCharacterForm(playerData, characterData);
        $('#edit_character_gm').modal();
    });
    $(document).on('change', '#character-classid', function(){
        var classId = $(this).val();
        refreshCharacterLists(0, classId);
    });
    $(document).on('submit', '#character-form', function (e) {
        e.preventDefault();
        var data = readCharacterForm();
        saveObject("character", data);
    });
    $(document).on('click', '.add_faction_gm, .edit_faction_gm', function(){
        var factionId = $(this).parents("p").data("faction-id");
        var data = getObjectData("faction", factionId);
        if(data == null){
            refreshFactionLists(0, null);
        }
        fillFactionForm(data);
        $('#edit_faction_gm').modal();
    });
    $(document).on('submit', '#faction-form', function (e) {
        e.preventDefault();
        var data = readFactionForm();
        saveObject("faction", data);
    });
    $(document).on('click', '.edit_province_gm', function(){
        var provinceId = $(this).parents("p").data("province-id");
        var data = getObjectData("province", provinceId);
        fillProvinceForm(data);
        $('#edit_province_gm').modal();
    });
    $(document).on('submit', '#province-form', function (e) {
        e.preventDefault();
        var data = readProvinceForm();
        saveObject("province", data);
    });
});

/**
 * Обновление списка персонажей
 *
 * @param {Number} characterId
 * @param {Number} classId
 * @param {Number} traitId
 * @param {Number} ambitionId
 */
function refreshCharacterLists(characterId, classId, traitId, ambitionId)
{
    var ambitions;
    var traits;
    if(!isEmpty(characterId)){
        ambitions = getAmbitionsByCharacterId(characterId);
        traits = getTraitsByCharacterId(characterId);
    } else {
        ambitions = getAmbitionsByClassId(classId);
        traits = getTraitsByClassId(classId);
    }
    createList("character-ambitionid", ambitionId, ambitions);
    createList("character-traitid", traitId, traits);
}
/**
 *
 * @param {Object} playerData
 * @param {Number} playerData.id
 * @param {String} playerData.username
 * @param {Object} characterData
 * @param {Number} characterData.id
 * @param {String} characterData.name
 * @param {Number} characterData.classId
 * @param {Number} characterData.traitId
 * @param {Number} characterData.ambitionId
 * @param {Number} characterData.provinceId
 */
function fillCharacterForm(playerData, characterData)
{
    if(playerData != null){
        $("#character_player_name").text(playerData.username);
        $("#character-playerid").val(playerData.id);
        if(characterData != null){
            $("#character-id").val(characterData.id);
            $("#character-name").val(characterData.name);
            $("#character-classid").val(characterData.classId);
            $("#character-provinceid").val(characterData.provinceId);
            refreshCharacterLists(characterData.id, characterData.classId, characterData.traitId, characterData.ambitionId);
        }
    } else {
        $("#character-playerid").val("");
        $("#character_player_name").html('<span class="alert-error">Игрок не найден!</span>');
    }
}
/**
 *
 * @returns {*|{name, value}|jQuery}
 */
function readCharacterForm()
{
    return $("#character-form").serializeArray();
}

/**
 * Обновление списка персонажей
 *
 * @param {number} factionId
 * @param {number} leaderId
 */
function refreshFactionLists(factionId, leaderId)
{
    var characters;
    characters = getObjectsList("characters", {"criteria": {"factionId": null}});
    if(!isEmpty(factionId)){
        var leader = getObjectData("character", leaderId);
        characters[leader.id] = leader.name;
    }
    createList("faction-leaderid", leaderId, characters);
}
/**
 * @param {Object} factionData Faction info
 * @param {Number} factionData.id Faction ID
 * @param {String} factionData.name Faction name
 * @param {Number} factionData.leaderId ID of leader character
 * @param {String} factionData.color Faction color
 */
function fillFactionForm(factionData)
{
    if(factionData != null){
        $("#faction-id").val(factionData.id);
        $("#faction-name").val(factionData.name);
        $("#faction-leaderid").val(factionData.leaderId);
        $("#faction-color").val(factionData.color)
        $("#faction-color-source").spectrum("set", factionData.color);
        refreshFactionLists(factionData.id, factionData.leaderId);
    }
}
function readFactionForm()
{
    return $("#faction-form").serializeArray();
}

function fillProvinceForm(data)
{
    if(data != null){
        $("#province-id").val(data.id);
        $("#province-name").val(data.name);
        $("#province-ownerid").val(data.ownerId);
    }
}
function readProvinceForm()
{
    return $("#province-form").serializeArray();
}