<?php
namespace app\modules\vestria;

use frontend\components\GameModule;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class VestriaModule extends GameModule
{
    public function init()
    {
        parent::init();

        // Register module messages
        Yii::$app->i18n->translations['vestria*'] = [
                    'class'          => 'yii\i18n\GettextMessageSource',
                    'basePath'       => '@frontend/modules/vestria/messages',
                    'catalog'        => 'vestria',
                    'sourceLanguage' => 'en'
                ];

        Yii::configure($this, require(__DIR__ . '/config/main.php'));

        $this->setAliases([
            '@vestria' => '@frontend/modules/vestria',
            '@moduleWebroot' => '@vestria/web'
        ]);
    }
}