<?php
namespace app\modules\vestria\models;

use frontend\components\JSONModel;
use Yii;

/**
 * Логгер хода
 *
 * @method Log setGame( Game $game )
 * @method Game getGame()
 * @method Log setTurn( int $turn )
 * @method int getTurn()
 * @method LogRow[] getRows()
 */

class Log extends JSONModel
{
    const VISIBILITY_GLOBAL = 1;
    const VISIBILITY_FACTION = 2;
    const VISIBILITY_PRIVATE = 3;

    /**
     * @var Game игра
     */
    protected $game;

    /**
     * @var int Номер хода
     */
    protected $turn;

    /** @var LogRow[] */
    protected $rows = [];

    /**
     * Конструктор модели
     *
     * @param Game $game
     * @param integer $turn
     */
    public function __construct( $game, $turn = 0 )
    {
        $this->game   = $game;
        $this->turn = $turn;
        $this->load();
    }

    /**
     * Загрузка игрового файла в модель
     */
    public function load()
    {
        $this->setPaths();

        $this->loadFromFile();
    }

    /**
     * Сохранение модели в файл
     *
     * @return bool
     */
    public function save()
    {
        $this->setPaths();

        return $this->saveToFile();
    }

    /**
     * Установка путей к папке и файлу
     */
    protected function setPaths()
    {
        $gameId = $this->game->getId();
        if ( ! empty( $gameId )) {
            $this->modelPath = Yii::getAlias('@frontend/modules') . "/vestria/web/data/games/" . $gameId . "/turns/" . (integer) $this->turn . "/";
            $this->modelFile = "turn_log.json";
        }
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "gameId" => $this->game->getId(),
            "turn"   => $this->turn,
            "rows"   => $this->rows,
        ];
    }

    /**
     * Загрузка сырых данных в свойства модели
     */
    protected function processRawData()
    {
        foreach ($this->rawData['rows'] as $data) {
            $this->rows[] = new LogRow( $this, $data );
        }
    }

    /**
     * @param string $text
     * @param int $characterId
     * @param int $visibility
     *
     * @return Log
     */
    public function addRow($text, $characterId = 0, $visibility = self::VISIBILITY_GLOBAL)
    {
        $row = new LogRow($this, [
            'text' => $text,
            'characterId' => $characterId,
            'visibility' => $visibility
        ]);
        $this->rows[] = $row;

        $this->save();

        return $this;
    }
}