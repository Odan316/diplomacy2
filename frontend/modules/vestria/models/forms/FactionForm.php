<?php
namespace app\modules\vestria\models\forms;

use yii\base\Model;
use Yii;

/**
 * Faction creation form
 */
class FactionForm extends Model
{
    public $id;
    public $name;
    public $leaderId;
    public $color;

    public function formName(){
        return 'Faction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],

            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['leaderId', 'required'],
            ['leaderId', 'integer'],

            ['color', 'required'],
            ['color', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => false,
            'name' => Yii::t('vestria', 'Title'),
            'leaderId' => Yii::t('vestria', 'Leader'),
            'color' => Yii::t('vestria', 'Color'),
        ];
    }

    /**
     * Saves data to user (create or update)
     *
     * @return boolean Saving result
     */
    public function apply()
    {
        if($this->validate()){
            $data = Yii::$app->request->post( "Faction", [ ] );
            if ( ! empty( $data['id'] )) {
                return Yii::$app->controller->game->updateFaction( $data );
            } else {
                $model = Yii::$app->controller->game->createFaction( $data );
                return (!empty($model));
            }
        } else {
            return 0;
        }
    }
}
