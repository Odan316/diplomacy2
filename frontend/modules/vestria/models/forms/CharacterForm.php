<?php
namespace app\modules\vestria\models\forms;

use yii\base\Model;
use Yii;

/**
 * Character creation form
 */
class CharacterForm extends Model
{
    public $playerId;
    /** @var int Character ID */
    public $id;
    public $name;
    public $classId;
    public $traitId;
    public $ambitionId;
    public $provinceId;

    public function formName(){
        return 'Character';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['playerId', 'required'],
            ['playerId', 'integer'],
            ['playerId', 'exist', 'targetClass' => '\common\models\User', 'targetAttribute' => 'id', 'message' => Yii::t('vestria', 'User doesn\'t exist')],

            ['id', 'integer'],

            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['classId', 'required'],
            ['classId', 'integer'],

            ['traitId', 'required'],
            ['traitId', 'integer'],

            ['ambitionId', 'required'],
            ['ambitionId', 'integer'],

            ['provinceId', 'required'],
            ['provinceId', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'playerId' => false,
            'id' => false,
            'name' => Yii::t('vestria', 'Name'),
            'classId' => Yii::t('vestria', 'Class'),
            'traitId' => Yii::t('vestria', 'Trait'),
            'ambitionId' => Yii::t('vestria', 'Ambition'),
            'provinceId' => Yii::t('vestria', 'Province'),
        ];
    }

    /**
     * Saves data to user (create or update)
     *
     * @return boolean Saving result
     */
    public function apply()
    {
        if($this->validate()){
            $data = Yii::$app->request->post( "Character", [ ] );
            if ( ! empty( $data['id'] )) {
                return Yii::$app->controller->game->updateCharacter( $data );
            } else {
                $model = Yii::$app->controller->game->createCharacter( $data );
                return (!empty($model));
            }
        } else {
            return 0;
        }
    }
}
