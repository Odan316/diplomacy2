<?php
namespace app\modules\vestria\models\forms;

use yii\base\Model;
use Yii;

/**
 * Province form
 */
class ProvinceForm extends Model
{
    public $id;
    public $name;
    public $ownerId;

    public function formName(){
        return 'Province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],

            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['ownerId', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => false,
            'name' => Yii::t('vestria', 'Title'),
            'ownerId' => Yii::t('vestria', 'Owner'),
        ];
    }

    /**
     * Saves data to user (create or update)
     *
     * @return boolean Saving result
     */
    public function apply()
    {
        if($this->validate()){
            $data = Yii::$app->request->post( "Province", [ ] );
            if ( ! empty( $data['id'] )) {
                return Yii::$app->controller->game->updateFaction( $data );
            } else {
                $model = Yii::$app->controller->game->createFaction( $data );
                return (!empty($model));
            }
        } else {
            return 0;
        }
    }
}
