<?php
namespace app\modules\vestria\models;

use app\modules\vestria\components\ModelsFinder;
use frontend\components\JSONModel;
use kartik\color\ColorInput;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class Parameter
 *
 * Класс параметра для действий
 *
 * @method Parameter setType( string $type )
 * @method string getType()
 * @method Parameter setName( string $name )
 * @method string getName()
 * @method Parameter setObject( string $object )
 * @method string getObject()
 * @method string[] getFilters()
 * @method mixed getMax()
 * @method mixed getMin()
 * @method Parameter setLabel( string $label )
 * @method string getLabel()
 * @method Parameter setValue( string $value )
 */
class Parameter extends JSONModel
{
    /** @var string */
    protected $type;
    /** @var string */
    protected $name;
    /** @var string */
    protected $object;
    /** @var string[] */
    protected $filters;
    /** @var int|[] */
    protected $max;
    /** @var int|[] */
    protected $min;
    /** @var string */
    protected $label;
    /**
     * Используется для скрытых полей, что бы задать им значение
     * @var mixed
     */
    protected $value;


    /**
     * @param Character $character
     * @param mixed $value
     *
     * @return string
     */
    public function getParameterCode( $character, $value)
    {
        $code = "";
        $wrapperClass = '';
        $htmlOptions = [ 'class' => 'request_parameter'];
        switch ($this->type) {
            case "objectsSelect":
                $objects = (new ModelsFinder($character->getGame()))->getObjects( $character, $this->object, $this->filters, true );
                $code = Html::dropDownList( $this->name, $value, ArrayHelper::map( $objects, "id", "name" ),
                    $htmlOptions );
                break;
            case "exactValue":
                if(!empty($this->max)){
                    $htmlOptions["max"] = $this->getValue($character, $this->max);
                }
                if(!empty($this->min)){
                    $htmlOptions["min"] = $this->getValue($character, $this->min);
                }
                $code = Html::textInput($this->name, $value, $htmlOptions);
                break;
            case "hiddenValue":
                $value = $this->getValue($character);
                $code = Html::hiddenInput($this->name, $value, $htmlOptions);
                break;
            case "colorSelect":
                $wrapperClass = 'colorpickerWrapper';
                $code = ColorInput::widget([
                    'name' => $this->name,
                    'value' => $value,
                    'options' => $htmlOptions,
                ]);
                break;
            default:
                break;
        }
        if($this->label != "")
            $code = "<label class='".$wrapperClass."'><span>".$this->label."</span>".$code."</label><br/>";

        return $code;
    }

    /**
     * @param Character|null $character
     * @param string $value
     *
     * @return mixed
     */
    protected function getValue($character = null, $value = null)
    {
        if(empty($value)) $value = $this->value;
        if(strpos($value, ".") && $character != null){
                $alias = explode(".", $value);
                $property = array_pop($alias);
                $model = (new ModelsFinder($character->getGame()))->getObject( $character, $alias[0]);
                $value = call_user_func( [ $model, "get".$property ] );
                return $value;
        } else
            return $value;
    }
}