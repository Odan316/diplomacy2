<?php
namespace app\modules\vestria\models;

use frontend\components\JSONModel;

/**
 * Class LogRow
 *
 * Класс отдельной смтроки в логе хода
 *
 * @method LogRow setCharacterId( int $characterId )
 * @method string getCharacterId()
 * @method LogRow setVisibility( int $visibility )
 * @method string getVisibility()
 * @method LogRow setText( string $text )
 * @method string getText()
 *
 */
class LogRow extends JSONModel
{
    /** @var string */
    protected $text;

    /** @var int */
    protected $characterId;

    /** @var int */
    protected $visibility;

    /** @var Log */
    protected $log;

    /**
     * @param Log $log
     * @param [] $data
     */
    public function __construct( $log, $data = [ ] )
    {
        $this->log = $log;
        parent::__construct( $data );
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'characterId' => $this->characterId,
            'visibility' => $this->visibility,
            'text' => $this->text
        ];
    }
}