<?php
return [
    [
        'class' => 'yii\web\GroupUrlRule',
        'prefix' => 'vestria',
        'rules' => [
            'game/<game:\d+>' => 'default/index',
            '<controller:\w+>/<action:[\w|\-]+>' => '<controller>/<action>',
            '<action:[\w|\-]+>' => 'default/<action>',
            '' => 'default/index',
        ],
    ]
];