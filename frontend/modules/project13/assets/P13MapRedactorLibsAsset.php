<?php

namespace app\modules\project13\assets;

use yii\web\AssetBundle;

/**
 * This is the libraries assets class for P13 Module.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class P13MapRedactorLibsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/';
    public $baseUrl = '@web/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];
    //public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    ];
    public $js = [
        'underscore/underscore-min.js'
    ];
    public $depends = [
    ];
}
