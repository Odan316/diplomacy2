<?php

namespace app\modules\project13\assets;

use yii\web\AssetBundle;

/**
 * This is the assets class for P13 Module, which contains common assets for Map.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class P13MapAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebRoot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'css/map.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\project13\assets\P13Asset',
    ];
}
