<?php

namespace app\modules\project13\assets;

use yii\web\AssetBundle;

/**
 * This is the base assets class for P13 Module.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class P13Asset extends AssetBundle
{
    public $sourcePath = '@moduleWebRoot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'css/styles.css',
    ];
    public $js = [
        'js/common.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\project13\assets\P13LibsAsset',
    ];
}
