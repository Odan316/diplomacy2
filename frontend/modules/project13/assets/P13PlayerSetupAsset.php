<?php

namespace app\modules\project13\assets;

use yii\web\AssetBundle;

/**
 * This is the assets class for P13 Module, which contains assets for Player Setup page.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class P13PlayerSetupAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebRoot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\project13\assets\P13Asset',
    ];
}
