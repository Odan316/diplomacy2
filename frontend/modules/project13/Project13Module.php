<?php
namespace app\modules\project13;

use frontend\components\GameModule;
use Yii;

class Project13Module extends GameModule
{
    public function init()
    {
        parent::init();

        // Register module messages
        Yii::$app->i18n->translations['p13*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@frontend/modules/project13/messages',
            'catalog'        => 'p13',
            'sourceLanguage' => 'en'
        ];

        Yii::configure($this, require(__DIR__ . '/config/main.php'));

        $this->setAliases([
            '@project13' => '@frontend/modules/project13',
            '@moduleWebRoot' => '@project13/web'
        ]);
    }
}