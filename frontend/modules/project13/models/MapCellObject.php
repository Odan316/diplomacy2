<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;
use Yii;

/**
 * Class MapCellObject
 *
 * Класс ячейки карты
 *
 * @method MapCellObject setLandObjectId( int $landObjectId )
 * @method int getLandObjectId()
 * @method MapCellObject setGfxVariantId( int $gfxVariantId )
 * @method int getGfxVariantId()
 *
 */
class MapCellObject extends JSONModel
{
    /** @var int */
    protected $landObjectId;
    /** @var int */
    protected $gfxVariantId;

    /** @var LandObject */
    protected $landObject;
    /** @var GFXVariant */
    protected $gfxVariant;

    /**
     * @return LandObject
     */
    public function getLandObject()
    {
        if (empty( $this->landObject ) || $this->landObject->getId() != $this->landObjectId) {
            $this->landObject = Yii::$app->controller->game->getConfig()->getConfigElementById( "character_classes", $this->landObjectId );
        }

        return $this->landObject;
    }

    /**
     * @return GFXVariant
     */
    public function getGfxVariant()
    {
        if (empty( $this->gfxVariant ) || $this->gfxVariant->getId() != $this->landObjectId) {
            $this->gfxVariant = $this->landObject->getGfxVariantById($this->gfxVariantId);
        }

        return $this->gfxVariant;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "landObjectId"   => $this->landObjectId,
            "gfxVariantId" => $this->gfxVariantId
        ];
    }
}