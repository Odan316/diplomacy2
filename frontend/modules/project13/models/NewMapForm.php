<?php
namespace app\modules\project13\models;

use Yii;
use yii\base\Model;


/**
 * Форма создания новой пустой карты
 */
class NewMapForm extends Model
{
    public $mapWidth;
    public $mapHeight;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mapWidth', 'filter', 'filter' => 'trim'],
            ['mapWidth', 'required'],
            ['mapWidth', 'integer', 'min' => 10, 'max' => 150],

            ['mapHeight', 'filter', 'filter' => 'trim'],
            ['mapHeight', 'required'],
            ['mapHeight', 'integer', 'min' => 10, 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mapWidth' => Yii::t('p13', 'Map Width'),
            'mapHeight' => Yii::t('p13', 'Map Height'),
        ];
    }

    /**
     * Создает объект новой пустой карты
     *
     * @return bool|null the saved model or null if saving fails
     */
    public function create()
    {
        if ($this->validate()) {
            /** @var Game $game */
            $game = Yii::$app->controller->game;
            return $game->getMap()->createBlankMap($this->mapWidth, $this->mapHeight);
        }

        return null;
    }
}
