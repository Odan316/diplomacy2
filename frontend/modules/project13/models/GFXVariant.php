<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;

/**
 * Class GFXVariant
 *
 * Класс графического варианта объекта на карте
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 *
 * @method LandObject setId( int $id )
 * @method int getId()
 * @method GFXVariant setTag( string $tag )
 * @method string getTag()
 * @method GFXVariant setGfx( string $gfx )
 * @method string getGfx()
 *
 */
class GFXVariant extends JSONModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $tag;
    /** @var string */
    protected $gfx;

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "id"   => $this->id,
            "tag" => $this->tag,
            "gfx" => $this->gfx
        ];
    }
}