<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;

/**
 * Class LandObject
 *
 * Класс объекта местности для карты (конфиг)
 *
 * @method LandObject setId( int $id )
 * @method int getId()
 * @method LandObject setName( string $name )
 * @method string getName()
 * @method LandObject setTag( string $tag )
 * @method string getTag()
 * @method LandObject setForEditor( bool $forEditor )
 * @method string getForEditor()
 *
 * @method GFXVariant[] getGfxVariants()
 *
 */
class LandObject extends JSONModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $name;
    /** @var string */
    protected $tag;
    /** @var bool */
    protected $forEditor;

    /** @var GFXVariant[] */
    protected $gfxVariants;

    /**
     * @inheritdoc
     */
    public function setAttributes( $data = [] )
    {
        if (isset( $data['gfxVariants'] )) {
            foreach ($data['gfxVariants'] as $gfxVariantData) {
                $this->gfxVariants[$gfxVariantData['id']] = new GFXVariant($gfxVariantData);
            }
            unset( $data['gfxVariants'] );
        }
        parent::setAttributes($data);
    }

    /**
     * @param int $id
     *
     * @return GFXVariant
     */
    public function getGfxVariantById( $id )
    {
        return $this->gfxVariants[$id];
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "id"   => $this->id,
            "name" => $this->name,
            "tag" => $this->tag,
            "forEditor" => $this->forEditor,
            "gfxVariants" => $this->gfxVariants
        ];
    }
}