<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;

/**
 * Class Character
 *
 * Класс ячейки карты
 *
 * @method MapCell setId( int $id )
 * @method int getId()
 * @method MapCell setX( int $x )
 * @method int getX()
 * @method MapCell setY( int $y )
 * @method int getY()
 *
 */
class MapCell extends JSONModel
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $x;
    /** @var int */
    protected $y;
    /** @var int */
    protected $landTypeId;
    /** @var MapCellObject[] */
    protected $cellObjects = [];

    /** @var LandType */
    protected $landType;

    /** @var Game */
    protected $game;
    /** @var Map */
    protected $map;

    /**
     * @param Map $map
     * @param array $data
     */
    public function __construct( $map, $data = [ ] )
    {
        $this->map = $map;
        $this->game = $this->map->getGame();


        if (isset( $data['cellObjects'] )) {
            foreach ($data['cellObjects'] as $cellObjectData) {
                $this->cellObjects[] = new MapCellObject( $cellObjectData );
            }
            unset( $data['cellObjects'] );
        }

        parent::__construct( $data );

        if(empty($this->id)){
            $this->id = $this->map->createCellId($this->x, $this->y);
        }

        $this->landType = $this->game->getConfig()->getConfigElementById( "land_types", $this->landTypeId );

    }

    public function addCellObject($cellObject)
    {
        $this->cellObjects[] = $cellObject;

        return $this;
    }

    public function removeCellObject($landObjectId, $gfxVariantId)
    {
        foreach($this->cellObjects as $index => $cellObject){
            if($cellObject->getLandObjectId() == $landObjectId && $cellObject->getGfxVariantId() == $gfxVariantId){
                unset($this->cellObjects[$index]);
                break;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "id"   => $this->id,
            "x" => $this->x,
            "y" => $this->y,
            "landTypeId" => $this->landTypeId,
            "cellObjects" => $this->cellObjects
        ];
    }
}