<?php
namespace app\modules\project13\models;

use frontend\components\GameInterface;
use frontend\components\JSONModel;
use Yii;

/**
 * Class Game
 * Модель для работы с игрой
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 *
 * @method GameConfig getConfig()
 * @method Map getMap()
 *
 * @method Game setId( int $id )
 * @method int getId()
 * @method Game setTurn( int $turn )
 * @method int getTurn()
 *
 * @method Game setLastTribeId( int $id )
 * @method int getLastTribeId()
 */
class Game extends JSONModel implements GameInterface
{
    /** @var int ИД игры */
    protected $id;
    /** @var int ИД хода */
    protected $turn;

    /** @var GameConfig Конфиг */
    protected $config;
    /** @var Map Карта */
    protected $map;

    protected $tribes = [];

    /** @var int */
    private $lastTribeId = 0;



    /**
     * Конструктор модели
     *
     * @param integer $gameId
     * @param integer $turn
     */
    public function __construct( $gameId, $turn = 0 )
    {
        $this->id   = $gameId;
        $this->turn = $turn;
        $this->load();
        $this->config = new GameConfig( $this->id );
        $this->map = new Map( $this );
    }

    /**
     * Загрузка игрового файла в модель
     */
    public function load()
    {
        $this->setPaths();

        $this->loadFromFile();
    }

    /**
     * Сохранение модели в файл
     *
     * @return bool
     */
    public function save()
    {
        //$this->map->save();

        $this->setPaths();

        return $this->saveToFile();
    }

    /**
     * Установка путей к папке и файлу
     */
    protected function setPaths()
    {
        if ( ! empty( $this->id )) {
            $this->modelPath = Yii::getAlias('@moduleWebRoot/data/games/') . $this->id . "/turns/" . (integer) $this->turn . "/";
            $this->modelFile = "main_save.json";
        }
    }

    /**
     * Загрузка сырых данных в свойства модели
     */
    protected function processRawData()
    {
        foreach ($this->rawData['tribes'] as $data) {
            //$this->provinces[] = new Province( $this, $data );
        }

    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'id'          => $this->id,
            'turn'        => $this->turn,
            'tribes' => $this->tribes,
            'lastTribeId' => $this->lastTribeId
        ];
    }

    /**
     * Создание новой игры
     */
    public function createNewGame()
    {
        $this->turn = 1;
        $this->save();
    }
}