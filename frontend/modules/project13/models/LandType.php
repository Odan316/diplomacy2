<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;

/**
 * Class LandType
 *
 * Класс Типа местости для карты (конфиг)
 *
 * @method LandType setId( int $id )
 * @method int getId()
 * @method LandType setName( string $name )
 * @method string getName()
 * @method LandType setTag( string $tag )
 * @method string getTag()
 * @method LandType setGfxColor( string $color )
 * @method string getGfxColor()
 *
 */

class LandType extends JSONModel
{
    /** @var  int */
    protected $id;
    /** @var  string */
    protected $name;
    /** @var  string */
    protected $tag;
    /** @var  string */
    protected $gfxColor;

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            "id"   => $this->id,
            "name" => $this->name,
            "tag" => $this->tag,
            "gfxColor" => $this->gfxColor
        ];
    }
}