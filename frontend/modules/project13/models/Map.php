<?php
namespace app\modules\project13\models;

use frontend\components\JSONModel;
use Yii;
use yii\helpers\VarDumper;

/**
 * Class P13Map
 *
 * Класс для работы с экземпляром карты Проекта 13
 *
 * @method int getWidth()
 * @method int getHeight()
 */

class Map extends JSONModel
{
    /** @var Game */
    private $game;

    /** @var int */
    protected $width;
    /** @var int */
    protected $height;

    /**
     * @var MapCell[] Массив ячеек карты
     */
    private $cells = [];

    /**
     * @param Game $game
     */
    public function __construct( $game )
    {
        $this->game = $game;
        $this->load();
    }

    /**
     * Загрузка игрового файла в модель
     */
    public function load()
    {
        $this->setPaths();

        $this->loadFromFile();
    }

    /**
     * Сохранение модели в файл
     *
     * @return bool
     */
    public function save()
    {
        $this->setPaths();

        return $this->saveToFile();
    }

    /**
     * Установка путей к папке и файлу
     */
    protected function setPaths()
    {
        if ( ! empty( $this->game )) {
            $this->modelPath = Yii::getAlias('@moduleWebRoot/data/games/') . $this->game->getId() . "/turns/" . (integer) $this->game->getTurn() . "/";
            $this->modelFile = "map.json";
        }
    }

    /**
     * Загрузка сырых данных в свойства модели
     */
    protected function processRawData()
    {
        $this->width = $this->rawData['width'];
        $this->height = $this->rawData['height'];

        foreach ($this->rawData['cells'] as $cellData) {
            if(isset($cellData['y']) && isset($cellData['x'])){
                $cell = new MapCell( $this, $cellData );
                $this->cells[$cell->getId()] = $cell;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'width' => $this->width,
            'height' => $this->height,
            'cells'  => $this->cells
        ];
    }

    /**
     * Создание чистой карты с заданными размерами
     *
     * @param integer $width
     * @param integer $height
     */
    public function createBlankMap($width, $height)
    {
        $this->width = $width;
        $this->height = $height;

        for($y = 1; $y <= $height; $y++) {
            for($x = 1; $x <= $width; $x++) {
                $cell = new MapCell( $this, [
                    'x' => $x,
                    'y' => $y
                ] );
                $this->cells[$cell->getId()] = $cell;
            }
        }
        //VarDumper::dump($this->cells, 5, 1);
        //die();
        $this->save();
    }

    /**
     * Загружает в модель дефолтную карту
     */
    public function loadDefaultMap()
    {
        /*$this->model_path = Yii::app()->getModulePath()."/project13/data/common/";
        $this->model_file = "default_map.json";

        $this->loadMap();*/
    }

    /**
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Указывает, создана ли карта или объект пуст
     *
     * @return bool
     */
    public function isCreated()
    {
        return !empty($this->cells);
    }

    /**
     * @param $x
     * @param $y
     *
     * @return int
     */
    public function createCellId($x, $y)
    {
        return $this->width*$y + $x;
    }
}