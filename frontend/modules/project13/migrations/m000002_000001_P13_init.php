<?php

use yii\db\Migration;

class m000002_000001_P13_init extends Migration
{
	public function safeUp()
	{
		$this->insert('{{%module}}', [
			'title' => 'Проект 13: Первобытность',
			'tag' => 'P13',
			'authorId' => 1,
			'systemName' => 'project13',
			'active' => true
		]);
	}

	public function safeDown()
	{
		$this->delete('{{%module}}', ['tag' => 'P13']);
	}
}