<?php
namespace app\modules\project13\controllers;

use app\modules\project13\components\P13Controller;
use app\modules\project13\models\NewMapForm;
use common\models\GameRole;
use Yii;

/**
 * Class DefaultController
 *
 * Контроллер для работы в Кабинете (Ведущего или Игрока)
 */
class DefaultController extends P13Controller
{
    /**
     * По умолчанию, в зависимости от роли мы грузим
     * - либо страницу ГМа (для ГМа),
     * - либо страницу игрока (для остальных)
     */
    public function actionIndex()
    {
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {
            return $this->actionGM();
        } else {
            return $this->actionPlayer();
        }
    }

    /**
     * Страница ГМа (только для ГМа)
     */
    public function actionGM()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {

            $areaData = []; //$game_data->map->getAreaInfo(35, 35, 80, 40);

            return $this->render( 'gm/index', [
                'game' => $this->game,
                'areaData' => $areaData
            ] );
        } else {
            return $this->actionAccessDenied();
        }
    }

    /**
     * Страница игрока
     */
    public function actionPlayer()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::PLAYER_ROLE) {

            $tribe = null;
            if(!$tribe){
                return $this->render( 'player/setup', [
                ] );
            } else {
                return $this->render( 'player/index', [
                    'tribe' => $tribe,
                ] );
            }
        } else {
            return $this->actionAccessDenied();
        }
    }

    public function actionMapRedactor()
    {
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {


            $newMapModel = new NewMapForm();
            if ($newMapModel->load(Yii::$app->request->post())) {
                $newMapModel->create();
            }

            /*$map = $this->game->getMap();

            if(!$map->exists() && isset($_POST['create_map'])){
                $map->createBlankMap(
                    htmlspecialchars($_POST['map_width']),
                    htmlspecialchars($_POST['map_height'])
                );
            }
            $mapInfo = $map->getMapInfo();
            $mapObjectTypes = $this->game->getConfig()->getConfigAsList( "land_obj" );*/

            return $this->render( 'map_redactor/index', [
                'game' => $this->game,
                'newMapModel' => $newMapModel
            ] );
        } else {
            return $this->actionAccessDenied();
        }
    }

    public function actionRules()
    {
        return $this->render( 'rules/index' );
    }

    /**
     * Отображение заглушки, говорящей что страница не доступна этой роли
     */
    public function actionAccessDenied()
    {
        return $this->render( 'access_denied' );
    }
}