<?php
/**
 * Class ConvertController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */


namespace app\modules\project13\controllers;


use yii\console\Controller;
use yii\helpers\VarDumper;

class ConvertController extends Controller
{


    public function actionMap()
    {
        $mapOldStr = file_get_contents(__DIR__.'/../web/data/common/old/default_map.json');
        $mapOld = json_decode($mapOldStr, true);

        $landObjConvertStr = file_get_contents(__DIR__.'/../web/data/common/old/landObjConvert.json');
        $landObjConvert = json_decode($landObjConvertStr, true);

        $mapNew = [
            'height'  => count($mapOld),
            'width' => count($mapOld[1]),
            'cells' => []
        ];
        foreach($mapOld as $y => $row){
            foreach($row as $x => $cell){
                $cellId = $mapNew['width']*$y + $x;
                $newCell = [
                    'id' => $cellId,
                    'x' => $x,
                    'y' => $y,
                    'landTypeId' => null,
                    'cellObjects' => []
                ];
                foreach($cell['objects'] as $oldObject){
                    if(in_array($oldObject['object_type'], $landObjConvert['landTypes'])){
                        $newCell['landTypeId'] = $landObjConvert['typeConvert'][$oldObject['object_type']];
                    } elseif(in_array($oldObject['object_type'], $landObjConvert['landObjects'])){
                        $newCell['cellObjects'][] = [
                            "landObjectId" => $landObjConvert['objectConvert'][$oldObject['object_type']],
                            "gfxVariantId" => $landObjConvert['objectGFXConvert'][$oldObject['object_gfx']]
                        ];
                    }
                }
                $mapNew['cells'][$cellId] = $newCell;
            }
        }
        $mapNewStr = json_encode($mapNew);
        file_put_contents(__DIR__.'/../web/data/common/defaultMap.json', $mapNewStr);
    }
}