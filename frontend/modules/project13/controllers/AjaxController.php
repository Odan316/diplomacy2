<?php
namespace app\modules\project13\controllers;

use app\modules\project13\components\P13Controller;
use Yii;
use yii\web\HttpException;


/**
 * Class AjaxController
 *
 * Контроллер для работы с AJAX-запросами
 */
class AjaxController extends P13Controller
{
    /**
     * @param \yii\base\Action $action
     *
     * @return $this|bool
     * @throws HttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return parent::beforeAction($action);
        } else {
            throw new HttpException(404, 'Document Does Not Exist');
        }
    }

    public function actionGetFullMapInfo()
    {
        return $this->game->getMap();
    }

    public function actionGetLandGfx()
    {
        return [
            'landTypes' => $this->game->getConfig()->getConfigAsObjectsArray('land_types'),
            'landObjects' => $this->game->getConfig()->getConfigAsObjectsArray('land_objects')
        ];
    }
}