<?php
/**
 * @var $this \yii\web\View
 * @var string $content
 */

use common\models\GameRole;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use app\modules\project13\assets\P13Asset;

P13Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        window.urlRoot = '<?= \Yii::$app->controller->module->params['moduleJSUrl']?>';
    </script>
</head>

<body>
    <?php $this->beginBody() ?>
    <div id="page" class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::t('p13', 'Project 13'),
            'brandUrl'   => ['/project13'],
            'options'    => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        $menuItemsLeft  = [];
        $menuItemsRight = [];
        if (Yii::$app->controller->getUserGameRole()->roleId == GameRole::MASTER_ROLE) {
            echo $this->render('_menu/_gm');
        } elseif (Yii::$app->controller->getUserGameRole()->roleId == GameRole::PLAYER_ROLE) {
            echo $this->render('_menu/_player');
        }

        ?>
        <?php NavBar::end(); ?>

        <div id="inner_content" class="container-fluid">
            <?= $content; ?>
        </div>

        <div class="clearfix"></div>

    </div>

    <div id="footer">
        "<?= Yii::$app->name ?>" Copyright by Onad &copy; <?= date('Y'); ?>. No Rights Reserved.
        <br/>
        <?= Yii::powered(); ?>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
