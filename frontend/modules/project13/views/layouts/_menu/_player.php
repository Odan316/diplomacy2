<?php
use yii\bootstrap\Nav;

$menuItemsLeft  = [
    [
        'label' => Yii::t('p13', 'Tribe'),
        'url'   => ['/tribe']
    ],
    [
        'label' => Yii::t('p13', 'Technologies'),
        'url'   => ['/tech']
    ],
    [
        'label' => Yii::t('p13', 'Request'),
        'url'   => ['/request']
    ],
    [
        'label' => Yii::t('p13', 'Map'),
        'url'   => ['/map']
    ],
    [
        'label' => Yii::t('p13', 'Statistics'),
        'url'   => ['/statistic']
    ],
    [
        'label' => Yii::t('p13', 'Rules'),
        'url'   => ['/project13/rules']
    ]
];
$menuItemsRight = [
    [
        'label'       => Yii::t('p13', 'Back to cabinet'),
        'url'         => ['/cabinet']
    ]
];
?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items'   => $menuItemsLeft,
]); ?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items'   => $menuItemsRight,
]); ?>