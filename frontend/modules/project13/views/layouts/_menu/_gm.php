<?php
use yii\bootstrap\Nav;

$menuItemsLeft  = [
    [
        'label' => Yii::t('p13', 'GM'),
        'url'   => ['/project13/gm']
    ],
    [
        'label' => Yii::t('p13', 'Map redactor'),
        'url'   => ['/project13/map-redactor']
    ],
    [
        'label' => Yii::t('p13', 'Map'),
        'url'   => ['/project13/map']
    ],
    [
        'label' => Yii::t('p13', 'Statistics'),
        'url'   => ['/project13/statistic']
    ],
    [
        'label' => Yii::t('p13', 'Rules'),
        'url'   => ['/project13/rules']
    ]
];
$menuItemsRight = [
    [
        'label'       => Yii::t('p13', 'Back to cabinet'),
        'url'         => ['/cabinet']
    ]
];
?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items'   => $menuItemsLeft,
]); ?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items'   => $menuItemsRight,
]); ?>