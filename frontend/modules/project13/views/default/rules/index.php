<?php
/**
 * @var $this \yii\web\View
 */

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'Rules');
?>
<div id="b_rules" class="container">
    <h1>"Проект 13: Первобытность" - Правила</h1>

    <?= \yii\bootstrap\Tabs::widget([
        'navType' => 'nav-pills',
        //'linkOptions' => ['class' => 'btn-xs'],
        'items'   => [
            [
                'active'  => true,
                'label'   => 'I. Общие понятия',
                'options' => ['id' => 'p1'],
                'content' => $this->render("_p1")
            ],
            [
                'label'   => 'II. Ход Игры',
                'options' => ['id' => 'p2'],
                'content' => $this->render("_p2")
            ],
            [
                'label'   => 'III. Племя',
                'options' => ['id' => 'p3'],
                'content' => $this->render("_p3")
            ],
            [
                'label'   => 'IV. Карта мира и миграция',
                'options' => ['id' => 'p4'],
                'content' => $this->render("_p4")
            ],
            [
                'label'   => 'V. Еда и прирост населения',
                'options' => ['id' => 'p5'],
                'content' => $this->render("_p5")
            ],
            [
                'label'   => 'VI. Войны',
                'options' => ['id' => 'p6'],
                'content' => $this->render("_p6")
            ],
            [
                'label'   => 'VII. Развитие и решения',
                'options' => ['id' => 'p7'],
                'content' => $this->render("_p7")
            ],
            [
                'label'   => 'VIII. Взаимодействие между племенами',
                'options' => ['id' => 'p8'],
                'content' => $this->render("_p8")
            ],
            [
                'label'   => 'IX. Окончание игры и победные очки.',
                'options' => ['id' => 'p9'],
                'content' => $this->render("_p9")
            ]
        ]
    ]); ?>

</div>
