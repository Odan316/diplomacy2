<?php
/**
 * @var $this \yii\web\View
 */

use app\modules\project13\assets\P13PlayerSetupAsset;

P13PlayerSetupAsset::register($this);

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'New tribe');
?>

Hello, new tribe