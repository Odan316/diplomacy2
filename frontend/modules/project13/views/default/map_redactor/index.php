<?php
/**
 * @var $this \yii\web\View
 * @var $game Game
 * @var $newMapModel NewMapForm
 */

use app\modules\project13\assets\P13MapAsset;
use app\modules\project13\assets\P13MapRedactorAsset;
use app\modules\project13\assets\P13MapRedactorLibsAsset;
use app\modules\project13\models\Game;
use app\modules\project13\models\LandObject;
use app\modules\project13\models\LandType;
use app\modules\project13\models\NewMapForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

P13MapRedactorLibsAsset::register($this);
P13MapAsset::register($this);
$redactorAssets = P13MapRedactorAsset::register($this);

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'Master cabinet');

$map = $game->getMap();
//\yii\helpers\VarDumper::dump($map, 5, 1)
?>
<div class="container-fluid">
    <?php if ($map->isCreated()) { ?>
        <div class="row">
            <div class="col-md-12">
                <?= Html::button(Yii::t('p13', 'Save'), ['id' => 'saveMapButton', 'class' => 'btn btn-primary']); ?>
            </div>
        </div>
        <div class="row">
            <div id="redactorMapOuter" class="col-md-8">
                <div id="redactorMap" class="p13map"
                     data-game-id="<?= $game->getId(); ?>"
                     style="width:<?= ( $map->getWidth() * 15 + 1 ) . 'px;'; ?>">
                    <?php for ($y = 1; $y <= $map->getHeight(); $y ++) { ?>
                        <div class="mapRow" id="y<?= $y ?>">
                            <?php for ($x = 1; $x <= $map->getWidth(); $x ++) { ?>
                                <div class="mapCell" id="cell<?= $map->createCellId($x, $y)?>" data-y="<?= $y ?>"
                                     data-x="<?= $x ?>"></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div id="redactorSideMenu" class="col-md-4">
                <div class="mapBrushesList">
                    <div class="mapBrushRow"
                         data-category="eraser">
                        <?= Html::img($redactorAssets->baseUrl . '/images/design/eraser.png',
                            ['class' => ['mapBrushIcon']]); ?>
                        <?= Yii::t('p13', 'Eraser') ?>
                    </div>
                </div>

                <h3><?= Yii::t('p13', 'Land types'); ?></h3>

                <div id="landTypesList" class="mapBrushesList">
                    <?php
                    /** @var LandType $landType */
                    foreach ($game->getConfig()->getConfigAsObjectsArray('land_types') as $landType) { ?>
                        <div class="mapBrushRow"
                             data-type="<?= $landType->getId() ?>"
                             data-category="landtype">
                            <div class="mapBrushIcon"
                                 style="background-color:<?= $landType->getGfxColor() ?>"></div>
                            <?= $landType->getName() ?>
                        </div>
                    <?php } ?>
                </div>

                <h3><?= Yii::t('p13', 'Land objects'); ?></h3>

                <div id="landObjectsList" class="mapBrushesList">
                    <?php
                    /** @var LandObject $landObject */
                    foreach ($game->getConfig()->getConfigAsObjectsArray('land_objects') as $landObject) { ?>
                        <?php if ($landObject->getForEditor()) { ?>
                            <div class="mapBrushRow"
                                 data-type="<?= $landObject->getId() ?>"
                                 data-category="landobj">
                                <?= Html::img(
                                    $redactorAssets->baseUrl . '/images/map_icons/' . $landObject->getGfxVariantById(1)->getTag() . '.png',
                                    ['class' => 'mapBrushIcon']); ?>
                                <?= $landObject->getName() ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <h3><?= Yii::t('p13', 'GFX Variants') ?></h3>

                <div id="object_gfx_list"></div>
            </div>
        </div>
    <? } else { ?>
        <h2><?= Yii::t('p13', 'There\'s no map in this game yet'); ?></h2>
        <div class="row">
            <div class="col-md-12">
                <?= Html::button(Yii::t('p13', 'Create blank map'),
                    ['id' => 'createBlankMap', 'class' => ['btn', 'btn-primary']]); ?>
                <?= Html::a(Yii::t('p13', 'Use base map'), ['default/create-default-map'],
                    ['class' => ['btn', 'btn-success']]); ?>
            </div>
        </div>
        <div id="blankMapCreation" class="row">
            <div class="col-md-4">
                <?php $form = ActiveForm::begin(['id' => 'form-create-map']); ?>
                <?= $form->field($newMapModel, 'mapWidth') ?>
                <?= $form->field($newMapModel, 'mapHeight') ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('p13', 'Create'),
                        ['class' => 'btn btn-primary', 'name' => 'create_map']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <? } ?>
</div>
