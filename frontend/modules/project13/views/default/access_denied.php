<?php
/**
 * @var $this DefaultController
 */

use app\modules\project13\controllers\DefaultController;

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'Access is denied');
?>
<h2><?= Yii::t('p13', 'Your role doesn\'t give you access to this page') ?></h2>