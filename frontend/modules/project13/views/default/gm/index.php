<?php
/**
 * @var $this \yii\web\View
 * @var $game Game
 */

use app\modules\project13\assets\P13MasterAsset;
use app\modules\project13\models\Game;

P13MasterAsset::register($this);

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'Master cabinet');
?>

<?=
\yii\bootstrap\Tabs::widget([
    'navType' => 'nav-pills',
    'items' => [
    ]
]);
?>

Hello, master