<?php
/**
 * Created by PhpStorm.
 * User: onag
 * Date: 14.07.15
 * Time: 22:26
 */

namespace frontend\controllers;


use common\models\Game;
use common\models\GameRole;
use common\models\GameStatus;
use common\models\Module;
use common\models\User;
use common\models\UserGameRole;
use frontend\models\GameCreationForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;

class CabinetController extends Controller
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @param int|null $game
     *
     * @return string
     */
    public function actionIndex($game = null)
    {
        $moduleCall = Yii::$app->getModule('hre');
        $user = User::findOne(Yii::$app->getUser()->getId());
        $modules = Module::find()->active()->all();
        if(!empty($game))
            $gameInfo = $this->actionGetGameInfo($game);
        else
            $gameInfo = '';

        return $this->render('index', [
            'user' => $user,
            'modules' => $modules,
            'gameHtml' => $gameInfo
        ]);
    }

    /**
     * @return string|static
     */
    public function actionCreateGame()
    {
        $model = new GameCreationForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($game = $model->create()) {
                return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $game->id]));
            }
        }

        if (Yii::$app->request->isAjax) {
            return $model->getErrors();
        } else {
            VarDumper::dump($model->getErrors(), 5, 1);
            return "Error";
        }
    }

    /**
     * @param int $gameId
     *
     * @return string|static
     */
    public function actionStartGame($gameId)
    {
        /** @var Game $game */
        $game = Game::findOne($gameId);
        $game->statusId = GameStatus::ACTIVE;
        $game->startedAt = date('U');
        $game->save();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }

    /**
     * @param int $gameId
     *
     * @return string|static
     */
    public function actionCancelGame($gameId)
    {
        /** @var Game $game */
        $game = Game::findOne($gameId);
        $game->statusId = GameStatus::CANCELLED;
        $game->endedAt = date('U');
        $game->save();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }

    /**
     * @param int $gameId
     *
     * @return string|static
     */
    public function actionEndGame($gameId)
    {
        /** @var Game $game */
        $game = Game::findOne($gameId);
        $game->statusId = GameStatus::FINISHED;
        $game->endedAt = date('U');
        $game->save();

        /** TODO: Create post-game scripts */

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }

    /**
     * @param int $gameId Game ID
     *
     * @return string|static
     */
    public function actionMakeRequest( $gameId )
    {
        $userRole          = new UserGameRole();
        $userRole->gameId = $gameId;
        $userRole->userId = Yii::$app->getUser()->getId();
        $userRole->roleId = GameRole::CLAIMER_ROLE;

        $userRole->save();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }

    /**
     * @param int $gameId Game ID
     *
     * @return string|static
     */
    public function actionAcceptRequest( $gameId, $userId )
    {
        $userRole          = UserGameRole::find()->byUser($gameId, $userId)->one();
        $userRole->roleId = GameRole::PLAYER_ROLE;

        $userRole->save();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }
    /**
     * @param int $gameId Game ID
     *
     * @return string|static
     */
    public function actionRefuseRequest( $gameId, $userId )
    {
        $userRole          = UserGameRole::find()->byUser($gameId, $userId)->one();
        $userRole->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->actionGetGameInfo($gameId);
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['cabinet/index', 'game' => $gameId]));
        }
    }

    /**
     * @param int $gameId Game ID
     *
     * @return string
     */
    public function actionGetGameInfo($gameId)
    {
        /** @var Game $gameModel */
        $gameModel = Game::findOne($gameId);
        $userRole = UserGameRole::find()
            ->where(['userId' => Yii::$app->getUser()->getId(), 'gameId' => $gameId])
            ->one();

        $viewParameters = [
            'game' => $gameModel,
            'master' => $gameModel->master,
            'players' => $gameModel->players,
            'claimers' => $gameModel->claimers,
            'userRole' => $userRole
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->renderAjax('_game_info', $viewParameters);
        } else {
            return $this->renderPartial('_game_info', $viewParameters);
        }
    }

    public function actionNo_such_game()
    {
        return $this->render( 'no_such_game' );
    }

}