<?php
namespace frontend\models;

use common\models\Person;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $nickname;
    public $mail;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('frontend', 'This username has already been taken.')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['nickname', 'filter', 'filter' => 'trim'],
            ['nickname', 'required'],
            ['nickname', 'unique', 'targetClass' => '\common\models\Person', 'message' => Yii::t('frontend', 'This nickname has already been taken.')],
            ['nickname', 'string', 'min' => 2, 'max' => 255],

            ['mail', 'filter', 'filter' => 'trim'],
            ['mail', 'required'],
            ['mail', 'email'],
            ['mail', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('frontend', 'This email has already been taken.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('frontend', 'Username'),
            'nickname' => Yii::t('frontend', 'Nickname'),
            'mail' => Yii::t('frontend', 'Email'),
            'password' => Yii::t('frontend', 'Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->mail = $this->mail;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole('user');
                $auth->assign($role, $user->getId());

                $person = new Person();
                $person->nickname = $this->nickname;
                $person->link("user", $user);
                $person->save();
                return $user;
            }
        }

        return null;
    }
}
