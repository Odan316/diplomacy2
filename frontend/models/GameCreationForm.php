<?php
namespace frontend\models;

use common\components\GameModule;
use common\models\Game;
use common\models\GameRole;
use common\models\GameStatus;
use common\models\Module;
use common\models\User;
use common\models\UserGameRole;
use frontend\components\GameInterface;
use yii\base\Model;
use Yii;

/**
 * Game creation form
 */
class GameCreationForm extends Model
{
    public $moduleId;
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'required'],
            [
                'title',
                'unique',
                'targetClass' => '\common\models\Game',
                'targetAttribute' => 'title',
                'message' => Yii::t('frontend', 'This title has already been taken.')
            ],
            ['title', 'string', 'min' => 2, 'max' => 255],

            ['moduleId', 'required'],
            ['moduleId', 'integer'],
            [
                'moduleId',
                'exist',
                'targetClass' => '\common\models\Module',
                'targetAttribute' => 'id',
                'message' => Yii::t('frontend', 'Module doesn\'t exist')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('frontend', 'Game title'),
            'moduleId' => Yii::t('frontend', 'Available modules')
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function create()
    {
        if ($this->validate()) {
            /** @var Module $module */
            $module = Module::findOne($this->moduleId);
            $sameModuleGamesCount = Game::find()->where(['moduleId' => $this->moduleId])->count('id');
            $game = new Game();
            $game->title = $this->title;
            $game->tag = $module->tag.(++$sameModuleGamesCount);
            $game->moduleId = $module->id;
            $game->statusId = GameStatus::OPEN_GAME;
            $game->createdAt = date('U');
            $game->lastTurn = 1;
            if ($game->save()) {
                $roleLink = new UserGameRole();
                $roleLink->userId = Yii::$app->getUser()->getId();
                $roleLink->gameId = $game->id;
                $roleLink->roleId = GameRole::MASTER_ROLE;
                if ($roleLink->save()) {

                    //$moduleClass = "\\app\\modules\\".$module->system_name."\\".ucfirst($module->system_name)."Module";
                    /** @var GameModule $moduleInstance */
                    $moduleInstance = Yii::$app->getModule($module->systemName);
                    //$moduleCall = $moduleClass::getInstance();
                    //die('mod requested');
                    //$gameClass = "\\common\\modules\\".$module->systemName."\\models\\Game";
                    /** @var GameInterface $moduleGame */
                    //$moduleGame = new $gameClass( $game->id, 0 );
                    $moduleInstance->createNewGame($game->id);
                    return $game;
                }
            }
        }

        return null;
    }
}
