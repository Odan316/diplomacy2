<?php
$params = array_merge(
    require( __DIR__ . '/../../common/config/params.php' ),
    require( __DIR__ . '/../../common/config/params-local.php' ),
    require( __DIR__ . '/params.php' ),
    require( __DIR__ . '/params-local.php' )
);

return [
    'id'                  => 'diplomacy-frontend',
    'name'                => 'DiploGame',
    'basePath'            => dirname(__DIR__),
    'homeUrl'             => '/',
    'bootstrap'           => [
        'log',
        'frontend\components\GameModuleBootstrap',
    ],
    'controllerNamespace' => 'frontend\controllers',
    // set target language to be Russian
    'language' => 'ru-RU',
    // set source language to be English
    'sourceLanguage' => 'en-GB',
    'components'          => [
        'request'      => [
            'baseUrl' => ''
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName'  => false,
            'rules' => [
                ''                                              => 'site/index',
                '<action:(signup|login|logout)>'                => 'site/<action>',
                '<controller:(cabinet)>/<game:\d+>'             => 'cabinet/index',
                '<controller:(cabinet|site)>'                   => '<controller>/index',
                '<controller:(cabinet|site)>/<action:[\w|\-]+>' => '<controller>/<action>'
            ],
        ],
    ],
    'aliases'             => [
        '@widgets' => '@frontend/components/widgets',
    ],
    'params'              => $params,
];
