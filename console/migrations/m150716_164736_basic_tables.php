<?php

use common\models\GameRole;
use common\models\GameStatus;
use console\components\SchemaHelper;
use yii\db\Schema;
use yii\db\Migration;

class m150716_164736_basic_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        /* Создание таблиц игр */
        $this->createTable('{{%module}}', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(36)->notNull(),
            'tag'        => $this->string(6)->notNull(),
            'authorId'   => $this->integer()->notNull(),
            'systemName' => $this->string(36)->notNull(),
            'active'     => $this->boolean()
        ], $tableOptions);
        $this->addForeignKey("module_author",
            "{{%module}}", "authorId",
            "{{%user}}", "id",
            'CASCADE', 'CASCADE'
        );

        $this->createTable('{{%gameStatus}}', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(36)->notNull(),
            'description' => $this->string(255)
        ], $tableOptions);
        $this->batchInsert('{{%gameStatus}}',
            ['id', 'title', 'description'],
            [
                [GameStatus::OPEN_GAME, 'Open', ''],
                [GameStatus::REGISTRATION_IS_OVER, 'Registration is over', ''],
                [GameStatus::ACTIVE, 'In progress', ''],
                [GameStatus::FINISHED, 'Finished', ''],
                [GameStatus::CANCELLED, 'Cancelled', ''],
                [GameStatus::DELETED, 'Deleted', ''],
            ]);

        $this->createTable('{{%game}}', [
            'id'        => $this->primaryKey(),
            'title'     => $this->string(36)->notNull(),
            'tag'       => $this->string(36)->notNull(),
            'moduleId'  => $this->integer()->notNull(),
            'statusId'  => $this->integer()->notNull(),
            'createdAt' => $this->integer()->notNull(),
            'startedAt' => $this->integer(),
            'endedAt'   => $this->integer(),
            'lastTurn'  => $this->integer()->notNull()->defaultValue(0)
        ], $tableOptions);
        $this->addForeignKey("game_module",
            "{{%game}}", "moduleId",
            "{{%module}}", "id",
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey("game_gameStatus",
            "{{%game}}", "statusId",
            "{{%gameStatus}}", "id",
            'CASCADE', 'CASCADE'
        );

        $this->createTable('{{%gameRole}}', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(36)->notNull(),
            'description' => $this->string(255)
        ], $tableOptions);
        $this->batchInsert('{{%gameRole}}',
            ['id', 'title', 'description'],
            [
                [GameRole::MASTER_ROLE, 'Game Master', ''],
                [GameRole::CLAIMER_ROLE, 'Claimer', ''],
                [GameRole::PLAYER_ROLE, 'Player', ''],
                [GameRole::BANNED_ROLE, 'Banned', '']
            ]);

        $this->createTable('{{%userGameRole}}', [
            'id'     => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'gameId' => $this->integer()->notNull(),
            'roleId' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey("userGameRole_user",
            "{{%userGameRole}}", "userId",
            "{{%user}}", "id",
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey("userGameRole_game",
            "{{%userGameRole}}", "gameId",
            "{{%game}}", "id",
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey("userGameRole_role",
            "{{%userGameRole}}", "roleId",
            "{{%gameRole}}", "id",
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('userGameRole_user', '{{%userGameRole}}');
        $this->dropForeignKey('userGameRole_game', '{{%userGameRole}}');
        $this->dropForeignKey('userGameRole_role', '{{%userGameRole}}');

        $this->dropTable('{{%userGameRole}}');
        $this->dropTable('{{%gameRole}}');

        $this->dropForeignKey('game_module', '{{%game}}');
        $this->dropForeignKey('game_gameStatus', '{{%game}}');

        $this->dropTable('{{%game}}');

        $this->dropTable('{{%gameStatus}}');

        $this->dropForeignKey('module_author', '{{%module}}');
        $this->dropTable('{{%module}}');
    }
}
