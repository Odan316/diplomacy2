<?php

use console\components\SchemaHelper;
use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('{{%user}}', [
            'id'                 => $this->primaryKey(),
            'username'           => $this->string()->notNull(),
            'authKey'            => $this->string(32)->notNull(),
            'passwordHash'       => $this->string()->notNull(),
            'passwordResetToken' => $this->string(),
            'mail'               => $this->string()->notNull(),
            'status'             => $this->smallInteger()->notNull()->defaultValue(10),
            'createdAt'          => $this->integer()->notNull(),
            'updatedAt'          => $this->integer()->notNull()
        ], $tableOptions);

        $this->createTable('{{%person}}', [
            'id'         => $this->primaryKey(),
            'userId'     => $this->integer()->notNull(),
            'nickname'   => $this->string(36)->notNull(),
            'name'       => $this->string(36),
            'surname'    => $this->string(36),
            'patronymic' => $this->string(36)
        ], $tableOptions);
        $this->addForeignKey("person_user",
            "{{%person}}", "userId",
            "{{%user}}", "id",
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('person_user', '{{%person}}');

        $this->dropTable('{{%person}}');
        $this->dropTable('{{%user}}');
    }
}
