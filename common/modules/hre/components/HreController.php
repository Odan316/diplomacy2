<?php
namespace common\modules\hre\components;

use common\models\Game as AppGame;
use common\models\GameRole;
use common\models\User;
use common\models\UserGameRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Cookie;

/**
 * Class P13Controller
 *
 * Переопределение класса контроллера под модуль
 *
 */
class HreController extends Controller
{
    /**
     * Модель пользователя (из базового движка)
     * @var User $userModel
     */
    protected $userModel;

    /**
     * Модель игры (из базового движка)
     * @var AppGame $gameModel
     */
    protected $gameModel;

    /**
     * Модель с ролью пользователя в игре
     * @var UserGameRole $userGameRole
     */
    protected $userGameRole;

    /**
     * Объект игры (из модуля)
     * @var Game
     */
    public $game;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Перед загрузкой контроллера необходимо
     * - установить общий layout модуля
     */
    public function init()
    {
        Yii::$app->language = 'ru-RU';

        $this->layout = 'main';

        parent::init();
    }

    /**
     * Перед загрузкой действия необходимо
     * - проверить наличие или попытаться установить ИД игры в куки
     * - проверить права пользователя на доступ к игре
     * - загрузить базовые модели пользователя и игры
     *
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $gameIdFromParam = Yii::$app->getRequest()->getQueryParam('game', 0);
        if ( ! empty( $gameIdFromParam )) {
            $gameId = $gameIdFromParam;
            $cookie = new Cookie([
                'name'   => 'gameId',
                'value'  => $gameId,
                'expire' => time() + 60 * 60 * 24 * 30
            ]);
            Yii::$app->response->cookies->add($cookie);
        } elseif (isset( Yii::$app->request->cookies['gameId'] )) {
            $gameId = Yii::$app->request->cookies->getValue('gameId');
        } else {
            return Yii::$app->getResponse()->redirect(Url::to(['/cabinet/no_such_game']));
        }

        //VarDumper::dump($gameId);

        $this->gameModel = AppGame::findOne($gameId);

        if ( ! $this->gameModel) {
            return Yii::$app->getResponse()->redirect(Url::to(['/cabinet/no_such_game']));
        }

        $this->userModel = User::findOne(Yii::$app->getUser()->getId());

        $this->userGameRole = UserGameRole::find()->byUser($gameId, Yii::$app->getUser()->getId())->one();
        if (empty( $this->userGameRole )) {
            $this->userGameRole = new UserGameRole([
                'userId' => $this->userModel,
                'gameId' => $gameId,
                'roleId' => GameRole::NO_ROLE
            ]);
        }

        //$this->game = new Game($this->gameModel->id, $this->gameModel->lastTurn);

        return parent::beforeAction($action);
    }

    public function getUserGameRole()
    {
        return $this->userGameRole;
    }
}