<?php
namespace common\modules\hre\controllers;

use common\models\GameRole;
use common\modules\hre\components\HreController;
use Yii;

/**
 * Class DefaultController
 *
 * Контроллер для работы в Кабинете (Ведущего или Игрока)
 */
class DefaultController extends HreController
{
    /**
     * По умолчанию, в зависимости от роли мы грузим
     * - либо страницу ГМа (для ГМа),
     * - либо страницу игрока (для остальных)
     */
    public function actionIndex()
    {
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {
            return $this->actionGM();
        } else {
            return $this->actionPlayer();
        }
    }

    /**
     * Страница ГМа (только для ГМа)
     */
    public function actionGM()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::MASTER_ROLE) {

            $areaData = []; //$game_data->map->getAreaInfo(35, 35, 80, 40);

            return $this->render( 'gm/index', [
                'game' => $this->game,
                'areaData' => $areaData
            ] );
        } else {
            return $this->actionAccessDenied();
        }
    }

    /**
     * Страница игрока
     */
    public function actionPlayer()
    {
        // Сначала проверяем роль
        if ($this->userGameRole->roleId == GameRole::PLAYER_ROLE) {

            $tribe = null;
            if(!$tribe){
                return $this->render( 'player/setup', [
                ] );
            } else {
                return $this->render( 'player/index', [
                    'tribe' => $tribe,
                ] );
            }
        } else {
            return $this->actionAccessDenied();
        }
    }

    public function actionRules()
    {
        return $this->render( 'rules/index' );
    }

    /**
     * Отображение заглушки, говорящей что страница не доступна этой роли
     */
    public function actionAccessDenied()
    {
        return $this->render( 'access_denied' );
    }
}