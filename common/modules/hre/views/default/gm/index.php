<?php
/**
 * @var $this \yii\web\View
 */

use common\modules\hre\assets\HreMasterAsset;

HreMasterAsset::register($this);

$this->title = Yii::$app->controller->module->title . ' - ' . Yii::t('hre', 'Master cabinet');
?>

<?=
\yii\bootstrap\Tabs::widget([
    'navType' => 'nav-pills',
    'items' => [
    ]
]);
?>

Hello, master