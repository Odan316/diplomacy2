<h2>VIII. Окончание игры и условия победы</h2>

<p>
    Игра заканчивается в одном из двух случаев - либо если один из игроков 4 хода подряд продержался на посту
    Императора, либо в случае окончания Междоусобной войны в пользу Узурпатора. После этого происходит подсчет победных
    очков и все государства, кол-во очков которых более или равно 150 считаются победившими. АИ-государства не
    участвуют в итоговом подсчете очков и не могут победить или проиграть.
</p>
<h3>Победные очки начисляются за:</h3>
<ul>
    <li>(суммарное ОП трех провинций, принадлежащих гос-ву и имеющих самое высокое ОП)/2</li>
    <li>(деньги, находившиеся в казне гос-ва на последний ход)/5</li>
    <li>численность постоянной ВС гос-ва</li>
    <li>(за число провинций, принадлежащих вассалам гос-ва)*3</li>
    <li>(число торговых договоров и династических браков гос-ва)*5</li>
    <li>титул Императора (20 очков)</li>
    <li>если СРИ состоит из 20 и более провинций, то каждое гос-во входящее в СРИ получает по 10 очков</li>
    <li>если СРИ состоит из 19 и менее провинций, то каждое гос-во не входящее в СРИ получает по 10 очков</li>
</ul>
<p>Победителями считаются государства, набравшие 25 и более очков</p>