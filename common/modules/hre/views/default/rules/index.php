<?php
/**
 * @var $this \yii\web\View
 */

$this->title = Yii::$app->controller->module->title . ' - ' . Yii::t('hre', 'Rules');
?>
<div id="b_rules" class="container">
    <h1><?= $this->title ?></h1>

    <?= \yii\bootstrap\Tabs::widget([
        'navType' => 'nav-pills',
        //'linkOptions' => ['class' => 'btn-xs'],
        'items'   => [
            [
                'active'  => true,
                'label'   => 'I. Общие понятия',
                'options' => ['id' => 'p1'],
                'content' => $this->render("_p1")
            ],
            [
                'label'   => 'II. Ход игры',
                'options' => ['id' => 'p2'],
                'content' => $this->render("_p2")
            ],
            [
                'label'   => 'III. Государства и Империя',
                'options' => ['id' => 'p3'],
                'content' => $this->render("_p3")
            ],
            [
                'label'   => 'IV. Параметры стран и провинций',
                'options' => ['id' => 'p4'],
                'content' => $this->render("_p4")
            ],
            [
                'label'   => 'V. Боевые действия',
                'options' => ['id' => 'p5'],
                'content' => $this->render("_p5")
            ],
            [
                'label'   => 'VI. Внутренняя политика и боевые тактики',
                'options' => ['id' => 'p6'],
                'content' => $this->render("_p6")
            ],
            [
                'label'   => 'VII. Дипломатия и феодальные отношения',
                'options' => ['id' => 'p7'],
                'content' => $this->render("_p7")
            ],
            [
                'label'   => 'VIII. Окончание игры и условия победы',
                'options' => ['id' => 'p8'],
                'content' => $this->render("_p8")
            ],
        ]
    ]); ?>

</div>
