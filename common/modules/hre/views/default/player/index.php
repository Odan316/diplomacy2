<?php
/**
 * @var $this \yii\web\View
 */

use app\modules\project13\assets\P13PlayerAsset;

P13PlayerAsset::register($this);

$this->title = Yii::t('p13', 'Project 13: Stone Age') . ' - ' . Yii::t('p13', 'Player cabinet');
?>
Hello, player