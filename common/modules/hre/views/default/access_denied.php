<?php
/**
 * @var $this DefaultController
 */

use app\modules\project13\controllers\DefaultController;

$this->title = Yii::$app->controller->module->title . ' - ' . Yii::t('hre', 'Access is denied');
?>
<h2><?= Yii::t('p13', 'Your role doesn\'t give you access to this page') ?></h2>