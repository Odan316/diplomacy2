<?php
use yii\bootstrap\Nav;

$menuItemsLeft  = [
    [
        'label' => Yii::t('hre', 'GM'),
        'url'   => ['/hre/gm']
    ],
    [
        'label' => Yii::t('hre', 'Statistics'),
        'url'   => ['/hre/statistic']
    ],
    [
        'label' => Yii::t('hre', 'Rules'),
        'url'   => ['/hre/rules']
    ]
];
$menuItemsRight = [
    [
        'label'       => Yii::t('hre', 'Back to cabinet'),
        'url'         => ['/cabinet']
    ]
];
?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items'   => $menuItemsLeft,
]); ?>
<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items'   => $menuItemsRight,
]); ?>