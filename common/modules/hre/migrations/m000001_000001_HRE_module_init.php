<?php

use yii\db\Migration;

class m000001_000001_HRE_module_init extends Migration
{
	public function safeUp()
	{
		$this->insert('{{%module}}', [
			'title' => 'Закат Империи',
			'tag' => 'HRE',
			'authorId' => 1,
			'systemName' => 'hre',
            'active' => true
		]);
	}

	public function safeDown()
	{
		$this->delete('{{%module}}', ['tag' => 'HRE']);
	}
}