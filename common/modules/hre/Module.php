<?php
namespace common\modules\hre;

use common\components\GameModule;
use Yii;

/**
 * Class Module
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
class Module extends GameModule
{
    public function init()
    {
        parent::init();

        // Register module messages
        Yii::$app->i18n->translations['hre*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@common/modules/hre/messages',
            'catalog'        => 'hre',
            'sourceLanguage' => 'en'
        ];

        Yii::configure($this, require(__DIR__ . '/config/main.php'));

        $this->setAliases([
            '@hre' => '@common/modules/hre',
            '@moduleWebRoot' => '@hre/web'
        ]);

        $this->title = Yii::t('hre', 'Empire');
    }

    /**
     * @param int $gameId
     * @return bool
     */
    public function createNewGame($gameId)
    {
        return true;
    }
}