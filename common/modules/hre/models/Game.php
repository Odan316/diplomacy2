<?php
namespace app\modules\project13\models;

use frontend\components\GameInterface;
use frontend\components\JSONModel;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Game
 * Модель для работы с игрой
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 *
 */
class Game extends ActiveRecord implements GameInterface
{
    /**
     * Создание новой игры
     */
    public function createNewGame()
    {
        return true;
    }
}