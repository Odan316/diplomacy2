<?php

namespace common\modules\hre\assets;

use yii\web\AssetBundle;

/**
 * This is the base assets class for HRE Module.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class HreAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebRoot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'css/styles.css',
    ];
    public $js = [
        'js/common.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\modules\hre\assets\HreLibsAsset',
    ];
}
