<?php

namespace common\modules\hre\assets;

use yii\web\AssetBundle;

/**
 * This is the assets class for HRE Module, which contains assets for Master Cabinet.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class HreMasterAsset extends AssetBundle
{
    public $sourcePath = '@moduleWebRoot';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'common\modules\hre\assets\HreAsset',
    ];
}
