<?php

namespace common\modules\hre\assets;

use yii\web\AssetBundle;

/**
 * This is the libraries assets class for HRE Module.
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class HreLibsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/';
    public $baseUrl = '@web/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];
    //public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
