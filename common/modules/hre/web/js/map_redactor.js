$(function(){
    $('#createBlankMap').on('click', function(){
        $('#blankMapCreation').show();
    });

    var mapRedactor = {
        map: {
            cells: {}
        },
        gfx:{
            landTypes:{},
            landObjects:{}
        },
        drawMap: function() {
            _.each(this.map.cells, function(mapCell, key){
                var cell = $('#cell'+mapCell.id);
                if(mapCell.landTypeId != null){
                    var landType = _.find(this.gfx.landTypes, function(landType){
                        return landType.id == mapCell.landTypeId;
                    });
                    cell.css('background-color', landType.gfxColor);
                }
                if(mapCell.cellObjects.length != 0){
                    _.each(mapCell.cellObjects, function(cellObject, key){
                        var landObject = _.find(this.gfx.landObjects, function(landObject){
                            return landObject.id == cellObject.landObjectId;
                        });
                        var gfxVariant = _.find(landObject.gfxVariants, function(gfxVariant){
                            return gfxVariant.id == cellObject.gfxVariantId;
                        });
                        $('<div class="mapObjectIcon '+gfxVariant.tag+'" />')
                            .appendTo(cell);
                    }, this);
                }
            }, this);
        }
    };
    if($('#redactorMap').length > 0){
        redactorDictsLoad();
        redactorMapLoad();
        mapRedactor.drawMap();
    }

    function redactorDictsLoad(){
        $.ajax({
            type: "POST",
            async: false,
            url: window.urlRoot+"ajax/get-land-gfx",
            dataType: 'json',
            data: {},
            success: function(gfx){
                mapRedactor.gfx = gfx;
            }
        });
    }

    function redactorMapLoad(){
        $.ajax({
            type: "POST",
            async: false,
            url: window.urlRoot+"ajax/get-full-map-info",
            dataType: 'json',
            data: {},
            success: function(map){
                mapRedactor.map = map;
            }
        });
    }
});