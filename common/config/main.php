<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache'       => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n'        => [
            'translations' => [
                'app*' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@common/messages',
                    'sourceLanguage' => 'en',
                ],
                'frontend*' => [
                    'class'          => 'yii\i18n\GettextMessageSource',
                    'basePath'       => '@common/messages',
                    'catalog'        => 'frontend',
                    'sourceLanguage' => 'en'
                ]
            ],
        ]
    ],
    'modules' => [
        'hre' => [
            'class' => 'common\modules\hre\Module',
            'type'  => 'gameModule'
        ],
    ]
];
