<?php

namespace common\components;

use Yii;
use yii\base\Exception;
use yii\base\Module;

/**
 * Class GameModule
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
class GameModule extends Module implements GameModuleInterface
{
    /** @var string */
    public $type;

    /** @var string Title for views */
    public $title;

    /**
     * @param integer $gameId
     * @return mixed
     * @throws Exception
     */
    public function createNewGame($gameId)
    {
        throw new Exception(Yii::t('app/common',
            'You must implement method [[createNewGame]] in each [[GameModule]] inheritor class'));
    }
}