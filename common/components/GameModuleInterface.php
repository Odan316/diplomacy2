<?php


namespace common\components;


/**
 * Interface GameModuleInterface
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
interface GameModuleInterface
{
    /**
     * @param integer $gameId
     * @return mixed
     */
    public function createNewGame($gameId);
}