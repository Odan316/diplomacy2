<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[GameRole]].
 *
 * @see GameRole
 */
class GameRoleQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return GameRole[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GameRole|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}