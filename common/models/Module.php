<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "module".
 *
 * @property integer $id
 * @property string $title
 * @property string $tag
 * @property integer $authorId
 * @property string $systemName
 * @property integer $active
 *
 * @property Game[] $games
 * @property User $author
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['authorId', 'active'], 'integer'],
            [['title', 'tag', 'systemName'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'title' => Yii::t('app/models', 'Title'),
            'tag' => Yii::t('app/models', 'Tag'),
            'authorId' => Yii::t('app/models', 'Author ID'),
            'systemName' => Yii::t('app/models', 'System Name'),
            'active' => Yii::t('app/models', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['moduleId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    /**
     * @inheritdoc
     * @return ModuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ModuleQuery(get_called_class());
    }
}
