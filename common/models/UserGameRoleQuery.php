<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UserGameRole]].
 *
 * @see UserGameRole
 */
class UserGameRoleQuery extends \yii\db\ActiveQuery
{
    /**
     * @param int $gameId
     * @param int $userId
     *
     * @return $this
     */
    public function byUser($gameId, $userId)
    {
        $this->andWhere(['gameId' => $gameId, 'userId' => $userId]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return UserGameRole[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserGameRole|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}