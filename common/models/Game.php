<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $title
 * @property string $tag
 * @property integer $moduleId
 * @property integer $statusId
 * @property string $createdAt
 * @property string $startedAt
 * @property string $endedAt
 * @property integer $lastTurn
 *
 * @property GameStatus $status
 * @property Module $module
 * @property User $master
 * @property User[] $players
 * @property User[] $claimers
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['moduleId', 'statusId', 'lastTurn', 'createdAt', 'startedAt', 'endedAt'], 'integer'],
            [['title', 'tag'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'title' => Yii::t('app/models', 'Title'),
            'tag' => Yii::t('app/models', 'Tag'),
            'moduleId' => Yii::t('app/models', 'Module ID'),
            'statusId' => Yii::t('app/models', 'Status ID'),
            'createdAt' => Yii::t('app/models', 'Create Date'),
            'startedAt' => Yii::t('app/models', 'Start Date'),
            'endedAt' => Yii::t('app/models', 'End Date'),
            'lastTurn' => Yii::t('app/models', 'Last Turn'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(GameStatus::className(), ['id' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'moduleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles()
    {
        return $this->hasMany(UserGameRole::className(), ['gameId' => 'id']);
    }

    /**
     * @return UserQuery
     */
    public function getMaster()
    {
        return $this->hasOne(User::className(), ['id' => 'userId'])
                    ->viaTable(UserGameRole::tableName(), ['gameId' => 'id'], function ($query) {
                        /* @var $query \yii\db\ActiveQuery */
                        $query->andWhere([UserGameRole::tableName().'.`roleId`' => GameRole::MASTER_ROLE]);
                    });
    }

    /**
     * @return UserQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(User::className(), ['id' => 'userId'])
                    ->viaTable(UserGameRole::tableName(), ['gameId' => 'id'], function ($query) {
                        /* @var $query \yii\db\ActiveQuery */
                        $query->andWhere([UserGameRole::tableName().'.`roleId`' => GameRole::PLAYER_ROLE]);
                    });
    }

    /**
     * @return UserQuery
     */
    public function getClaimers()
    {
        return $this->hasMany(User::className(), ['id' => 'userId'])
                    ->viaTable(UserGameRole::tableName(), ['gameId' => 'id'], function ($query) {
                        /* @var $query \yii\db\ActiveQuery */
                        $query->andWhere([UserGameRole::tableName().'.`roleId`' => GameRole::CLAIMER_ROLE]);
                    });
    }

    /**
     * @inheritdoc
     * @return GameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameQuery(get_called_class());
    }
}
