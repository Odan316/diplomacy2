<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Game]].
 *
 * @see Game
 */
class GameQuery extends \yii\db\ActiveQuery
{

    /*
     * @return GameQuery
     */
    public function hasUser($userId)
    {
        $this
            ->leftJoin(UserGameRole::tableName(), UserGameRole::tableName().'.`gameId` = '.Game::tableName().'.`id`')
            ->andWhere([UserGameRole::tableName().'.`userId`' => $userId]);
        return $this;
    }

    /*
     * @return GameQuery
     */
    public function dontHasUser()
    {
        $gamesWithUser = Game::find()->hasUser(Yii::$app->getUser()->getId())->all();
        $ids = ArrayHelper::map($gamesWithUser, 'id', 'id');

        $this->andWhere(['not in', Game::tableName() . '.`id`', $ids]);
        return $this;
    }

    /*
     * @return GameQuery
     */
    public function open()
    {
        $this->andWhere(['in', Game::tableName() . '.`statusId`', [GameStatus::OPEN_GAME]]);
        return $this;
    }

    /*
     * @return GameQuery
     */
    public function active()
    {
        $this->andWhere(['not in', Game::tableName() . '.`statusId`', [GameStatus::FINISHED, GameStatus::CANCELLED]]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Game[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Game|array|null
     */
    public function one($db = null)
    {
        $this->limit(1);
        return parent::one($db);
    }
}