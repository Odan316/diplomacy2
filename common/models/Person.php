<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "person".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $nickname
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 *
 * @property User $user
 */
class Person extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['nickname', 'name', 'surname', 'patronymic'], 'string', 'max' => 255],
            [['nickname'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app/models', 'ID'),
            'userId'     => Yii::t('app/models', 'User ID'),
            'nickname'   => Yii::t('app/models', 'Nickname'),
            'name'       => Yii::t('app/models', 'Name'),
            'surname'    => Yii::t('app/models', 'Surname'),
            'patronymic' => Yii::t('app/models', 'Patronymic'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @inheritdoc
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonQuery(get_called_class());
    }
}
