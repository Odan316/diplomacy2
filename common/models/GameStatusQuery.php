<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[GameStatus]].
 *
 * @see GameStatus
 */
class GameStatusQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return GameStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GameStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}