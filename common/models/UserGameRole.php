<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "userGame".
 *
 * @property integer $id
 * @property string $userId
 * @property integer $gameId
 * @property integer $roleId
 */
class UserGameRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%userGameRole}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'gameId', 'roleId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'userId' => Yii::t('app/models', 'User ID'),
            'gameId' => Yii::t('app/models', 'Game ID'),
            'roleId' => Yii::t('app/models', 'Role ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserGameRoleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserGameRoleQuery(get_called_class());
    }
}
