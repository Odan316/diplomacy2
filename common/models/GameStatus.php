<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gameStatus".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Game[] $games
 */
class GameStatus extends \yii\db\ActiveRecord
{
    const OPEN_GAME = 1;
    const REGISTRATION_IS_OVER = 2;
    const ACTIVE = 3;
    const FINISHED = 10;
    const CANCELLED = 11;
    const DELETED = 90;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gameStatus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'title' => Yii::t('app/models', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['statusId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GameStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameStatusQuery(get_called_class());
    }
}
