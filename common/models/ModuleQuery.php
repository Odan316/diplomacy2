<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Module]].
 *
 * @see Module
 */
class ModuleQuery extends \yii\db\ActiveQuery
{
    /**
     * @return ModuleQuery
     */
    public function active()
    {
        $this->andWhere(['active' => true]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Module[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Module|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}