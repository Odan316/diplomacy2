<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gameRole".
 *
 * @property integer $id
 * @property string $title
 */
class GameRole extends \yii\db\ActiveRecord
{
    const NO_ROLE = 1;
    const MASTER_ROLE = 1;
    const CLAIMER_ROLE = 2;
    const PLAYER_ROLE = 3;
    const BANNED_ROLE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gameRole}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'title' => Yii::t('app/models', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     * @return GameRoleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameRoleQuery(get_called_class());
    }
}
