<?php
return [
    'It is recommended you use an upgraded browser to display the {type} control properly.'
    => 'Вам следует обновить браузер, что бы элемент типа "{type}" отображался и работал правильно'
];